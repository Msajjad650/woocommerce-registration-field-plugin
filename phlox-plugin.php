<?php

/**
 * @package PhloxPlugin
 */

/*
 Plugin Name: Woocommerce Registration Form Custom Fields
 Plugin URI: https://wp-crafts.com
 Description: Obtain additional information from your customers on registration form with Woocommerce Registration Form Custom Fields Plugin. You can also enable customers to login using different social networks. This Plugin supports more than 10 different types of custom fields such as text box, text area, select box, multi select box, checkbox, radio button, etc.
 Version: 1.0
 Author: WP Crafts
 Author URI: https://wp-crafts.com
 License: GPLv2 or later
*/

/*
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the free software foundation; either version 2 of the license.
 */

defined('ABSPATH') or die('Access Denied');

if(file_exists(dirname(__FILE__).'/vendor/autoload.php')){
	require_once dirname(__FILE__).'/vendor/autoload.php';
}

//initializing constants
define('PLUGIN_PATH', plugin_dir_path(__FILE__));
define('PLUGIN_URL', plugin_dir_url( __FILE__ ));
define('PLUGIN', plugin_basename( __FILE__ ));

//check if woocommerce plugin is installed
if (!in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
	add_action( 'admin_notices', 'phlox_error_notice_wc_not_installed' );
}
//Show error if woocommerce not installed
function phlox_error_notice_wc_not_installed(){
	?>
	<div class="error notice">
	    <p><strong>Error! </strong>This plugin required woocommerce installed!</p>
	</div>
	<?php
}

//activate plugin
function activate_phlox_plugin(){
	Inc\Base\Activate::activate();
}
register_activation_hook( __FILE__, 'activate_phlox_plugin' );

//deactivate plugin
function deactivate_phlox_plugin(){
	Inc\Base\Deactivate::deactivate();
}
register_deactivation_hook( __FILE__, 'deactivate_phlox_plugin' );


//ininitalizing plugin
if (class_exists('Inc\\Init')) {
	Inc\Init::register_services();
	add_action( 'plugins_loaded', 'phlox_init');
}

//getting woocommerce countries
function phlox_init(){
	if ( class_exists( 'WC_Countries' ) ) {
		add_filter( 'woocommerce_integrations', 'phlox_add_integration');
	} else {
		//echo "string";
	}
}

function phlox_add_integration(){
	$integrations[] = 'WC_Countries';
	return $integrations;
}
