<?php
/**
 * @package PhloxPlugin
 */

namespace Inc\Base;
class Activate{
	//adding tables when activating plugin.
	public static function activate(){
		flush_rewrite_rules();
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		self::phlox_create_reg_default_fields_table();
		self::phlox_create_reg_field_table();
		self::phlox_create_account_info();
	}

	public function phlox_create_reg_default_fields_table(){
		global $wpdb;
		$prefix = $wpdb->prefix;
		$table_name = $prefix.'phlox_reg_default_fields';
		if (count($wpdb->get_var('SHOW TABLES LIKE "$table_name"')) == 0){
			$create_table_query = "CREATE TABLE `".$table_name."` (
					  `field_id` int(25) NOT NULL,
					  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
					  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
					  `placeholder` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
					  `is_required` int(25) NOT NULL,
					  `width` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
					  `sort_order` int(25) NOT NULL,
					  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
					  `mode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
					  `status` text COLLATE utf8mb4_unicode_ci,
					   `message` text COLLATE utf8mb4_unicode_ci
					) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci"; 

			dbDelta($create_table_query);
			self::phlox_insert_into_default_fields();
		}
	}

	public function phlox_insert_into_default_fields(){
		global $wpdb;
		$prefix = $wpdb->prefix;
		$table_name = $prefix.'phlox_reg_default_fields';
		$wpdb->query('TRUNCATE TABLE '.$table_name);
		$query = "INSERT INTO `".$table_name."` (`field_id`, `name`, `label`, `placeholder`, `is_required`, `width`, `sort_order`, `type`, `mode`, `status`,`message`) VALUES
		(1, 'first_name', 'First Name', '', 1, 'half', 1, 'text', 'de_registration', 'disabled',''),
		(2, 'last_name', 'Last Name', '', 1, 'half', 2, 'text', 'de_registration', 'disabled',''),
		(3, 'billing_company', 'Company', '', 0, 'full', 5, 'text', 'de_registration', 'disabled',''),
		(4, 'billing_country', 'Country', '', 1, 'full', 6, 'select', 'de_registration', 'disabled',''),
		(5, 'billing_address_1', 'Street Address', '', 1, 'full', 7, 'text', 'de_registration', 'disabled',''),
		(6, 'billing_address_2', 'Address 2', '', 0, 'full', 8, 'text', 'de_registration', 'disabled',''),
		(7, 'billing_city', 'Town / City', '', 1, 'full', 9, 'text', 'de_registration', 'disabled',''),
		(8, 'billing_state', 'State / County', '', 1, 'full', 10, 'select', 'de_registration', 'disabled',''),
		(9, 'billing_postcode', 'Postcode / Zip', '', 1, 'half', 11, 'text', 'de_registration', 'disabled',''),
		(10, 'billing_phone', 'Phone', '', 1, 'half', 12, 'phn', 'de_registration', 'disabled','')";

		$wpdb->query($query);
	}

	public function phlox_create_reg_field_table(){
		global $wpdb;
		$prefix = $wpdb->prefix;
		$table_name = $prefix.'phlox_reg_fields';
		if (count($wpdb->get_var('SHOW TABLES LIKE "$table_name"')) == 0){
			$create_table_query = "CREATE TABLE `".$table_name."` (
				 `field_id` int(25) NOT NULL AUTO_INCREMENT,
				 `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
				 `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
				 `placeholder` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
				 `is_required` int(25) NOT NULL,
				 `is_hide` int(25) NOT NULL,
				 `is_readonly` int(25) NOT NULL,
				 `width` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
				 `sort_order` int(25) NOT NULL,
				 `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
				 `mode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
				 `message` text COLLATE utf8mb4_unicode_ci,
				 `showif` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
				 `condition_field` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
				 `pcondition` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
				 `condition_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
				 `checkbox_text` text COLLATE utf8mb4_unicode_ci,
				 PRIMARY KEY (`field_id`)
				) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci"; 

			dbDelta($create_table_query);
		}
	}

	public function phlox_create_account_info(){
		global $wpdb;
		$prefix = $wpdb->prefix;
		$table_name = $prefix.'phlox_meta_settings';
		if (count($wpdb->get_var('SHOW TABLES LIKE "$table_name"')) == 0){
			$create_table_query = "CREATE TABLE `".$table_name."` (
					 `acc_id` int(11) NOT NULL AUTO_INCREMENT,
					 `meta_name` varchar(225) NOT NULL,
					 `meta_data` text NOT NULL,
					 PRIMARY KEY (`acc_id`)
					) ENGINE=InnoDB DEFAULT CHARSET=latin1"; 

			dbDelta($create_table_query);
			self::phlox_insert_meta_settings();
		}
	}

	function phlox_insert_meta_settings(){
		global $wpdb;
		$prefix = $wpdb->prefix;
		$table_name = $prefix.'phlox_meta_settings';

		$wpdb->query('TRUNCATE TABLE '.$table_name);

		$general_settings['account_title'] = '';
		$general_settings['profile_title'] = '';
		$gen_json = json_encode($general_settings);

		$fb_settings['fb_status'] = 'disabled';
		$fb_settings['fb_app'] = '';
		$fb_settings['fb_secret'] = '';
		$fb_json = json_encode($fb_settings);

		$twitter_settings['twitter_status'] = 'disabled';
		$twitter_settings['twitter_consumer_key'] = '';
		$twitter_settings['twitter_consumer_secret'] = '';
		$twitter_settings['twitter_callback_url'] = '';
		$twitter_json = json_encode($twitter_settings);

		$recaptcha_settings['recaptcha_site'] = '';
		$recaptcha_settings['recaptcha_secret'] = '';
		$recaptcha_json = json_encode($recaptcha_settings);

		$gplus_settings['gplus_status'] = 'disabled';
		$gplus_settings['gplus_client_id'] = '';
		$gplus_settings['gplus_client_secret'] = '';
		$gplus_settings['gplus_login_url'] = '';
		$gplus_json = json_encode($gplus_settings);

		$query = "INSERT INTO `".$table_name."` (`meta_name`, `meta_data`) VALUES
		('general_settings', '".$gen_json."'),
		('fb_settings', '".$fb_json."'),
		('twitter_settings', '".$twitter_json."'),
		('recaptcha_settings', '".$recaptcha_json."'),
		('gplus_settings', '".$gplus_json."')";

		$wpdb->query($query);
	}
}