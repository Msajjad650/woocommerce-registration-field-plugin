<?php
/**
 * @package PhloxPlugin
 */

 namespace Inc\Base;

 class Enqueue
 {
 	//Including styles and js files
 	public function register(){
 		add_action( 'admin_enqueue_scripts', array($this, 'enqueue') );
 	}
 	 function enqueue(){
    	wp_enqueue_style( 'phloxpluginstyle', PLUGIN_URL.'assets/style.css');
    	wp_enqueue_script( 'phloxpluginscript', PLUGIN_URL.'assets/script.js');
    	wp_enqueue_style('phloxpluginfontawesome', 'https://use.fontawesome.com/releases/v5.3.1/css/all.css');
    }
 }