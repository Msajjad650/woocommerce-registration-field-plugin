<?php
/**
 * @package PhloxPlugin
 */


namespace Inc\Pages;
class Admin{

	public function register(){

		add_action( 'admin_menu', array($this, 'add_admin_pages'));
		
        add_action('wp_ajax_phlox_insert_field', array($this, 'phlox_insert_field'));
        add_action('wp_ajax_phlox_delete_field', array($this, 'phlox_delete_field'));
        add_action('wp_ajax_phlox_save_all_fields', array($this, 'phlox_save_all_fields'));
        add_action('wp_ajax_phlox_save_default_fields', array($this, 'phlox_save_default_fields'));
        add_action('wp_ajax_phlox_save_all_settings', array($this, 'phlox_save_all_settings'));
        add_action('wp_ajax_phlox_update_sortorder', array($this, 'phlox_update_sortorder'));
        
        add_action( 'edit_user_profile', array($this, 'phlox_admin_profile_fields' ));
		add_action( 'edit_user_profile_update', array($this, 'phlox_admin_update_profile_fields' ));
	}

    //adding menu and sub menus
	public function add_admin_pages(){
    	add_menu_page( 
    		'Phlox Plugin',
    	 	'Registration Fields', 
    	 	'manage_options', 
    	 	'phlox_plugin', 
    	 	array($this, 'admin_index'), 
    	 	PLUGIN_URL.'images/wp_crafts_icon.png', 
    	 	110 
    	 );
    	add_submenu_page( 
    		'phlox_plugin', 
    		'Default Fields', 
    		'Default Fields', 
    		'manage_options', 
    		'default-fields',
    		array($this, 'default_fields_page'),
    		''
    	);
        add_submenu_page( 
            'phlox_plugin', 
            'Settings', 
            'Settings', 
            'manage_options', 
            'pps-settings',
            array($this, 'phlox_fields_setting'),
            ''
        );
    }

    //custom field view page
    public function admin_index(){
    	require_once PLUGIN_PATH.'templates/admin.php';
    }

    //default fields view page
    public function default_fields_page(){
    	require_once PLUGIN_PATH.'templates/def_fields.php';
    }

    //plugin setting page
    public function phlox_fields_setting() {
        require_once PLUGIN_PATH.'templates/pps_settings.php';
    }

    //insert custom field in db and show in front end
    function phlox_insert_field(){
        global $wpdb;
        $table_name = $wpdb->prefix.'phlox_reg_fields';
        $last1 = $wpdb->get_row("SHOW TABLE STATUS LIKE '$table_name'");
        $a = ($last1->Auto_increment);
        if(isset($_POST['fieldtype']) && $_POST['fieldtype']!='') {
            $fieldtype = sanitize_text_field($_POST['fieldtype']);
        } else { $fieldtype = '';}
        if(isset($_POST['type']) && $_POST['type']!='') {
            $type = sanitize_text_field($_POST['type']);
        } else { $type = ''; }
        $checkbox_text = ''; 
        $name = 'registration_field_'.$a;
        $mode = ''; 
        $fieldmessage = '';
        $showif = '';
        $ccondition = ''; 
        $ccondition_value = '';

        //check if captcha field is already included
        if ($fieldtype == 'captcha') {
            $getfield = $wpdb->get_results($wpdb->prepare("SELECT field_id FROM $table_name WHERE type = '".$fieldtype."'","")); 
            if (count($getfield) > 0) {
                $data['html'] = '';
                $data['classid'] = '';
                $data['status'] = 'not';
                echo json_encode($data);
                die();  
            }   
        }

        //insert field and show return the html to front
        if($fieldtype!='' && $type!='') {
            $wpdb->query($wpdb->prepare("INSERT INTO $table_name
            (name, label, type, mode, message, showif, condition_field, pcondition, condition_value, checkbox_text) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
            $name, '', $fieldtype, $mode, $fieldmessage, $showif, $name, $ccondition, $ccondition_value, $checkbox_text) );

            $last = $wpdb->get_row("SHOW TABLE STATUS LIKE '$table_name'");

            $insert_id = json_encode(($last->Auto_increment)-1);
            //get fields data
            $all_fields = self::get_all_fields($insert_id);
            $html = '';
            $html.='<li id="'.$insert_id.'" class="ui-sortable-handle" style="list-style: none;">
                <a class="phlox-collapsible" id="'.$insert_id.'" onclick="toggleField(this)">Registration Field '. $insert_id .'</a>
                <div class="content" id="'.$insert_id.'">
                    <p>
                        <label>Label: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                        <input type="text" name="'. $insert_id .'[label]" class="phlox-input">
                    </p>';
            if($fieldtype == 'checkboxa'){
                $html.='<p>
                    Note: This field is for just user acceptance like terms and conditions, this field will not be saved or sent in order or emails etc. You can add anchor tag to link with other page.
                </p>
                <p>
                    <label>Required: </label>
                    <input type="checkbox" value="1" name="'. $insert_id .'[required]">
                </p>
                <p>
                    <label>Text: </label><br>
                    <textarea name="'. $insert_id .'[checkbox_text]" style="width: 100%;"></textarea>
                </p>';
            }elseif($fieldtype == 'captcha'){
                $html.='<p class="impnote">Note: Before use this fields enter google captcha api Credentials in module setting tab.</p>';
            }else{
                $html.='<p>
                    <label>Required: </label>
                    <input type="checkbox" value="1" name="'. $insert_id .'[required]">
                </p>
                <p>
                    <label>Hide: </label>
                    <input type="checkbox" value="1" name="'. $insert_id .'[hidden]">
                </p>
                <p>
                    <label>Read Only(Do not allow customers to edit in myaccount): </label>
                    <input type="checkbox" value="1" name="'. $insert_id .'[readonly]">
                </p>';
            }
            if($fieldtype == 'select' || $fieldtype == 'multiselect' || $fieldtype == 'radio'){
                $html.='<div class="phlox_field_wrapper">
                    <div id="phlox-options-'.$insert_id.'">
                        <input class="phlox-optval-input" placeholder="Option Value" type="text" name="'.$insert_id.'[optionValue][]">
                        <input class="phlox-optval-input" placeholder="Option Text" type="text" name="'.$insert_id.'[optionText][]">
                        <i class="fas fa-plus-circle fa-2x phlox-font-awesome" onclick="addOption('.$insert_id.')"></i>                            
                    </div>
                </div>';
            }else{
                if($fieldtype != 'checkbox' && $fieldtype != 'checkboxa'){
                    $html.='<p>
                        <label>Placeholder: </label>
                        <input type="text" name="'. $insert_id .'[placeholder]" class="phlox-input">
                    </p>';
                }
            }
            $html.='
                <p>
                    <label>Field Width: </label>
                    <select class="phlox-input" name="'. $insert_id .'[width]">
                        <option value="full">Full Width</option>
                        <option value="half">Half Width</option>
                    </select>
                </p>
                <p>
                    <label>Conditional Logic: </label>
                    <div>
                        <select name="'. $insert_id .'[showif]">
                            <option>Select</option>
                            <option value="show">Show</option>
                            <option value="hide">Hide</option>
                        </select>
                        <strong>If value of </strong>
                        <select name="'. $insert_id .'[conditionField]" class="phlox-fcond">
                            <option>Select</option>
                            '. $all_fields .'
                        </select>
                        <select name="'. $insert_id .'[condition]">
                            <option value="">Select</option>
                            <option value="not-empty">is not empty</option>
                            <option value="equal-to">is equal to</option>
                            <option value="not-equal-to">is not equal to</option>
                            <option value="checked">is checked</option>                         
                        </select>
                        <input type="text" name="'. $insert_id .'[conditionValue]">
                    </div>
                </p>
                <p><a href="javascript:void(0)" onclick="deleteThis('.$insert_id.')">Delete</a></p>
                <p id="'.$insert_id.'"><input type="hidden" name="'. $insert_id .'[type]" value="'. $fieldtype .'" /></p>
            </div></li>';
                       
        }
        $data['html'] = $html;
        $data['classid'] = $insert_id;
        $data['status'] = 'ok';
        echo json_encode($data);
        die();
    }

    //get all fields and show them in select
    function get_all_fields($insert_id){
        global $wpdb;
        $table_name = $wpdb->prefix.'phlox_reg_fields';
        $getfields = $wpdb->get_results($wpdb->prepare("SELECT field_id,name,label FROM $table_name WHERE field_id != $insert_id",""));
        $ret_data = '';
        foreach ($getfields as $fields) {
            $ret_data.='<option value="'.$fields->field_id.'">';
            if($fields->label != ''){ $ret_data.= $fields->label; }else{ $ret_data.= $fields->name; }
            $ret_data.='</option>';
        }
        return $ret_data;
    }

    //delete field
    function phlox_delete_field(){
        global $wpdb;
        $table_name = $wpdb->prefix.'phlox_reg_fields';
        $field_id = $_POST['fieldId'];
        if($wpdb->query("DELETE FROM $table_name WHERE `field_id` = $field_id")){
            echo "ok";
        }else{
            echo "not";
        }
        die();
    }

    //save all feilds
    function phlox_save_all_fields(){
        global $wpdb;
        $table_name = $wpdb->prefix.'phlox_reg_fields';
        unset($_POST['action']);

        foreach ($_POST as $fieldId => $data) {
            if(isset($data['label']) && $data['label'] != ''){
                $label = $data['label'];
            }else{ $label = ''; }
            if (isset($data['required']) && $data['required'] != '') {
                $required = $data['required'];
            }else{ $required = '0'; }
            if (isset($data['hidden']) && $data['hidden'] != '') {
                $hidden = $data['hidden'];
            }else{ $hidden = '0'; }
            if (isset($data['readonly']) && $data['readonly'] != '') {
                $readonly = $data['readonly'];
            }else{ $readonly = '0'; }
            if (isset($data['placeholder']) && $data['placeholder'] != '') {
                $placeholder = $data['placeholder'];
            }else{ $placeholder = ''; }
            if (isset($data['width']) && $data['width'] != '') {
                $width = $data['width'];
            }else{ $width = ''; }
            if (isset($data['showif']) && $data['showif'] != '') {
                $showif = $data['showif'];
            }else{ $showif = ''; }
            if (isset($data['conditionField']) && $data['conditionField'] != '') {
                $condition_field = $data['conditionField'];
            }else{ $condition_field = ''; }
            if (isset($data['condition']) && $data['condition']) {
                $condition = $data['condition'];
            }else{ $condition = ''; }
            if (isset($data['conditionValue']) && $data['conditionValue']) {
                $condition_value = $data['conditionValue'];
            }else{ $condition_value = ''; }
            if (isset($data['checkbox_text']) && $data['checkbox_text'] != '') {
                $checkbox_text = $data['checkbox_text'];
            }else{ $checkbox_text = ''; }

            if (isset($data['optionValue']) && $data['optionValue'][0] != '' && isset($data['optionText']) && $data['optionText'][0] != '') {
                self::update_meta_data($fieldId, $data['optionValue'], $data['optionText']);
            }
            
            $wpdb->query($wpdb->prepare("UPDATE $table_name SET label = %s, placeholder = %s, 
                is_required = %d, is_hide = %d, width = %s, showif = %s, 
                condition_field = %s, pcondition = %s, condition_value = %s, is_readonly = %s, checkbox_text = %s WHERE field_id = %d",
                stripslashes($label),
                stripslashes($placeholder),
                $required,
                $hidden,
                $width,
                $showif,
                $condition_field,
                $condition,
                stripslashes($condition_value),
                $readonly,
                stripslashes($checkbox_text),
                $fieldId
            ));
        }
        echo 'done';
        die();
    }

    //save fields metadata
    function update_meta_data($fieldId, $optionValue, $optionText){
        global $wpdb;
        $table_name = $wpdb->prefix.'phlox_meta_settings';
        $wpdb->query("DELETE FROM $table_name WHERE `meta_name` = $fieldId");
        $meta_data = array();
        $count = 0;
        foreach ($optionValue as $value) {
            $meta_data[$optionText[$count]] = $value;
            $count++;
        }
        $wpdb->query($wpdb->prepare( 
            "INSERT INTO $table_name
            (meta_name, meta_data)
            VALUES (%s, %s)
            ",
            $fieldId, 
            json_encode($meta_data)
            ) );
    }

    //get all fields
    static function get_all_record(){
        global $wpdb;
        $table_name = $wpdb->prefix.'phlox_reg_fields';
        $getfields = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name ORDER BY sort_order ASC",""));
        return $getfields;
    }

    //save fields in asc or desc order
    function phlox_update_sortorder(){
        global $wpdb;
        $fieldids = $_POST['fieldids'];
        $counter = 1;
        $table_name = $wpdb->prefix.'phlox_reg_fields';
        foreach ($fieldids as $fieldid) {
            $wpdb->query($wpdb->prepare("UPDATE $table_name SET sort_order = %d WHERE field_id = %d", $counter, intval($fieldid) ));            
            $counter = $counter + 1;    
        }
    }

    //return fields metadata
    static function get_field_meta_data($field_id){
        global $wpdb;
        $table_name = $wpdb->prefix.'phlox_meta_settings';
        $getfields_meta = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `meta_name` = $field_id",""));
        return $getfields_meta;
    }

    //returns all default fields data
    static function get_all_default_record(){
        global $wpdb;
        $table_name = $wpdb->prefix.'phlox_reg_default_fields';
        $getfields = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name",""));
        return $getfields;
    }

    //save all defaults fields
    function phlox_save_default_fields(){
        global $wpdb;
        $table_name = $wpdb->prefix.'phlox_reg_default_fields';
        unset($_POST['action']);
       //print_r($_POST);exit;
        foreach ($_POST as $fieldId => $data) {
            if(isset($data['label']) && $data['label'] != ''){
                $label = $data['label'];
            }else{ $label = ''; }
            if (isset($data['required']) && $data['required'] != '') {
                $required = $data['required'];
            }else{ $required = '0'; }
            if (isset($data['placeholder']) && $data['placeholder'] != '') {
                $placeholder = $data['placeholder'];
            }else{ $placeholder = ''; }
            if (isset($data['width']) && $data['width'] != '') {
                $width = $data['width'];
            }
            if (isset($data['status']) && $data['status'] != '') {
                $status = $data['status'];
            }else{ $status = '0'; }
            
           
            $wpdb->query($wpdb->prepare("UPDATE $table_name SET label = %s, placeholder = %s, 
                is_required = %d, width = %s, status = %s WHERE field_id = %d",
                stripslashes($label),
                stripslashes($placeholder),
                $required,
                $width,
                $status,
                $fieldId
            ));
        }
        echo 'done';
        die();
    }

    //returns all metadata
    static function phlox_get_all_meta_data(){
        global $wpdb;
        $table_name = $wpdb->prefix.'phlox_meta_settings';
        $getsettings = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name",""));
        return $getsettings;
    }

    //save all plugin settings, fb, twitter, gplus etc
    function phlox_save_all_settings(){
        global $wpdb;
        $table_name = $wpdb->prefix.'phlox_meta_settings';
        unset($_POST['action']);
        //print_r($_POST);exit;

        $wpdb->query('TRUNCATE TABLE '.$table_name);

        $general_settings['account_title'] = $_POST['account_title'];
        $general_settings['profile_title'] = $_POST['profile_title'];
        $gen_json = json_encode($general_settings);

        $fb_settings['fb_status'] = $_POST['fb_status'];
        $fb_settings['fb_app'] = $_POST['fb_app'];
        $fb_settings['fb_secret'] = $_POST['fb_secret'];
        $fb_json = json_encode($fb_settings);

        $twitter_settings['twitter_status'] = $_POST['twitter_status'];
        $twitter_settings['twitter_consumer_key'] = $_POST['twitter_consumer_key'];
        $twitter_settings['twitter_consumer_secret'] = $_POST['twitter_consumer_secret'];
        $twitter_settings['twitter_callback_url'] = $_POST['twitter_callback_url'];
        $twitter_json = json_encode($twitter_settings);

        $recaptcha_settings['recaptcha_site'] = $_POST['recaptcha_site'];
        $recaptcha_settings['recaptcha_secret'] = $_POST['recaptcha_secret'];
        $recaptcha_json = json_encode($recaptcha_settings);
        
        $gplus_settings['gplus_status'] = $_POST['gplus_status'];
        $gplus_settings['gplus_client_id'] = $_POST['gplus_client_id'];
        $gplus_settings['gplus_client_secret'] = $_POST['gplus_client_secret'];
        $gplus_settings['gplus_login_url'] = $_POST['gplus_login_url'];
        $gplus_json = json_encode($gplus_settings);

        $query = "INSERT INTO `".$table_name."` (`meta_name`, `meta_data`) VALUES
        ('general_settings', '".$gen_json."'),
        ('fb_settings', '".$fb_json."'),
        ('twitter_settings', '".$twitter_json."'),
        ('recaptcha_settings', '".$recaptcha_json."'),
        ('gplus_settings', '".$gplus_json."')";

        $wpdb->query($query);
        echo "done";
        die();
    }
    
    function phlox_get_select_options($field_id){
		global $wpdb;
        $table_name = $wpdb->prefix.'phlox_meta_settings';
        return $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `meta_name` = '$field_id'",""));
	}
    
    //show all fields on customer profile page
    function phlox_admin_profile_fields(){ ?>
        <h3><?php echo _e('Customer Profile', 'phlox'); ?></h3>
        <table class="form-table">
            <tbody>
                <?php
                    //get all fields and show them in user profile page in admin panel 
                    $fields = self::get_all_record();
                    if(count($fields) > 0){
                        foreach($fields as $field){
                            $value = get_user_meta( $_GET['user_id'], $field->name, true );

							if($field->type == 'text' && $field->is_hide == 0) { ?>

								<tr id="a<?php echo $field->name; ?>" >
									<th><label for="<?php echo $field->name; ?>"><?php echo $field->label; ?></label></th>
									<td>
										<input type="text" class="regular-text" value="<?php echo $value; ?>" id="<?php echo $field->name; ?>" name="<?php echo $field->name; ?>">
										<br>
										<span class="description"></span>
										<?php if(isset($field->message) && $field->message!='') { ?>
											<span class="description"><?php echo $field->message; ?></span>
										<?php } ?>
									</td>
								</tr>

							<?php } else if($field->type == 'textarea' && $field->is_hide == 0) { ?>

								<tr id="a<?php echo $field->name; ?>" >
									<th><label for="<?php echo $field->name; ?>"><?php echo $field->label; ?></label></th>
									<td>
										<textarea cols="30" rows="5" id="<?php echo $field->name; ?>" name="<?php echo $field->name; ?>"><?php echo $value; ?></textarea>
										<br>
										<span class="description"></span>
										<?php if(isset($field->message) && $field->message!='') { ?>
											<span class="description"><?php echo $field->message; ?></span>
										<?php } ?>
									</td>
								</tr>

							<?php } else if($field->type == 'select' && $field->is_hide == 0) { ?>

								<tr id="a<?php echo $field->name; ?>" >
									<th><label for="<?php echo $field->name; ?>"><?php echo $field->label; ?></label></th>
									<td>
										<select style="min-width:200px;" id="<?php echo $field->name; ?>" name="<?php echo $field->name; ?>">
										    <option value="">Please select value</option>
											<?php $options = self::phlox_get_select_options($field->field_id);
											    if(isset($options[0]->meta_data)){
											        
											    $options = json_decode($options[0]->meta_data, true);
												foreach($options as $key => $value) { ?>
												
												<option value="<?php echo $key; ?>" <?php if($key == $value) { echo "selected"; } ?>>
													<?php echo $value; ?>
												</option>
												
											<?php } } ?>
											
										</select>
										<br>
										<span class="description"></span>
										<?php if(isset($field->message) && $field->message!='') { ?>
											<span class="description"><?php echo $field->message; ?></span>
										<?php } ?>
									</td>
								</tr>

							<?php } else if($field->type == 'multiselect' && $field->is_hide == 0) { ?>

								<tr id="a<?php echo $field->name; ?>" > 
									<th><label for="<?php echo $field->name; ?>"><?php echo $field->label; ?></label></th>
									<td>
										<select style="min-width:200px;min-height:150px;" multiple="true" id="<?php echo $field->name; ?>" name="<?php echo $field->name; ?>[]">
											
											<?php 
											    $options = self::phlox_get_select_options($field->field_id);
											    if(isset($options[0]->meta_data)){
                    								$options = json_decode( $options[0]->meta_data, true );
                    								$selected_vals = explode(',', $value);
                    								$new_arr = array();
                    								foreach ($selected_vals as $key => $valu) {
                    									$new_arr[] = trim($valu);
                    								}
                    								
                    								foreach($options as $option=>$val) { ?>
                    								    <option value="<?php echo $option; ?>" <?php if(in_array($option, $new_arr)) { echo "selected"; } ?>><?php echo $val; ?></option>
                							        <?php } 
                							    } 
                						    ?>
                							
										</select>
										<br>
										<span class="description"></span>
										<?php if(isset($field->message) && $field->message!='') { ?>
											<span class="description"><?php echo $field->message; ?></span>
										<?php } ?>
									</td>
								</tr>

							<?php } else if($field->type == 'checkbox' && $field->is_hide == 0) { ?>

								<tr id="a<?php echo $field->name; ?>" > 
									<th><label for="<?php echo $field->name; ?>"><?php echo $field->label; ?></label></th>
									<td>
										<input type="checkbox" name="<?php echo $field->name; ?>" id="c<?php echo $field->name; ?>" value="<?php echo $value; ?>" <?php if($value == 1) { echo "checked"; } ?> />
										<br>
										<span class="description"></span>
										<?php if(isset($field->message) && $field->message!='') { ?>
											<span class="description"><?php echo $field->message; ?></span>
										<?php } ?>
									</td>
								</tr>

							<?php } else if($field->type == 'radio' && $field->is_hide == 0) { ?>

								<tr id="a<?php echo $field->name; ?>" > 
									<th><label for="<?php echo $field->name; ?>"><?php echo $field->label; ?></label></th>
									<td>
										<?php 
										$options = self::phlox_get_select_options($field->field_id);
										if(isset($options[0]->meta_data)){
										    foreach($options as $option) { ?>
										
										        <input type="radio" name="<?php echo $field->name; ?>" id="<?php echo $field->name; ?>" value="<?php echo $option->meta_key; ?>" <?php if($option->meta_key == $value) { echo "checked"; } ?> /> <?php echo $option->meta_value; ?>
										
										    <?php } 
									    } ?>
										<br>
										<span class="description"></span>
										<?php if(isset($field->message) && $field->message!='') { ?>
											<span class="description"><?php echo $field->message; ?></span>
										<?php } ?>
									</td>
								</tr>

							<?php } else if($field->type == 'datepicker' && $field->is_hide == 0) { ?>

								<tr id="a<?php echo $field->name; ?>" >
									<th><label for="<?php echo $field->name; ?>"><?php echo $field->label; ?></label></th>
									<td>
										<input type="text" class="regular-text datepick" value="<?php echo $value; ?>" id="<?php echo $field->name; ?>" name="<?php echo $field->name; ?>">
										<br>
										<span class="description"></span>
										<?php if(isset($field->message) && $field->message!='') { ?>
											<span class="description"><?php echo $field->message; ?></span>
										<?php } ?>
									</td>
								</tr>

							<?php } else if($field->type == 'timepicker' && $field->is_hide == 0) { ?>

								<tr id="a<?php echo $field->name; ?>" >
									<th><label for="<?php echo $field->name; ?>"><?php echo $field->label; ?></label></th>
									<td>
										<input type="text" class="regular-text timepick" value="<?php echo $value; ?>" id="<?php echo $field->name; ?>" name="<?php echo $field->name; ?>">
										<br>
										<span class="description"></span>
										<?php if(isset($field->message) && $field->message!='') { ?>
											<span class="description"><?php echo $field->message; ?></span>
										<?php } ?>
									</td>
								</tr>

							<?php } else if($field->type == 'password' && $field->is_hide == 0) { ?>

								<tr id="a<?php echo $field->name; ?>" >
									<th><label for="<?php echo $field->name; ?>"><?php echo $field->label; ?></label></th>
									<td>
										<input type="password" class="regular-text" value="<?php echo $value; ?>" id="<?php echo $field->name; ?>" name="<?php echo $field->name; ?>">
										<br>
										<span class="description"></span>
									</td>
								</tr>

							<?php } else if($field->type == 'file' && $field->is_hide == 0) { ?>

								<tr id="a<?php echo $field->name; ?>" >
									<th><label for="<?php echo $field->name; ?>">Current <?php echo $field->label; ?></label></th>
									<td>
									<?php 
						
									$aa = $value;
									$ext = pathinfo($aa, PATHINFO_EXTENSION);
									if($ext == 'pdf' || $ext == 'PDF') { ?>
										<a href="<?php echo $value; ?>" target="_blank">
											<img src="<?php echo PLUGIN_URL; ?>images/pdf.png" width="100" height="100" title="Click to View" />
										</a>
									<?php } else { ?>
										<img src="<?php echo $value; ?>" width="100" height="100" />
									<?php } ?>
										<input type="hidden" class="regular-text" value="<?php echo $value; ?>" id="curr_<?php echo $field->name; ?>" name="curr_<?php echo $field->name; ?>">
									</td>
								</tr>

								<tr id="a<?php echo $field->name; ?>" >
									<th><label for="<?php echo $field->name; ?>"><?php echo $field->label; ?></label></th>
									<td>
										<input type="file" class="regular-text" id="<?php echo $field->name; ?>" name="<?php echo $field->name; ?>">
										<br>
										<span class="description"></span>
										<?php if(isset($field->message) && $field->message!='') { ?>
											<span class="description"><?php echo $field->message; ?></span>
										<?php } ?>
									</td>
								</tr>

							<?php } else if($field->type == 'numeric' && $field->is_hide == 0) { ?>

								<tr id="a<?php echo $field->name; ?>" >
									<th><label for="<?php echo $field->name; ?>"><?php echo $field->label; ?></label></th>
									<td>
										<input type="number" class="regular-text" value="<?php echo $value; ?>" id="<?php echo $field->name; ?>" name="<?php echo $field->name; ?>">
										<br>
										<span class="description"></span>
										<?php if(isset($field->message) && $field->message!='') { ?>
											<span class="description"><?php echo $field->message; ?></span>
										<?php } ?>
									</td>
								</tr>

							<?php } else if($field->type == 'color') {  ?>

								<tr id="a<?php echo $field->name; ?>" >
									<th><label for="<?php echo $field->name; ?>"><?php echo $field->label; ?></label></th>
									<td>
										<?php //echo $value; ?>
										<input type="text" class="regular-text color_spectrum" value="<?php echo $value; ?>" id="<?php echo $field->name; ?>" name="<?php echo $field->name; ?>">
										<br>
										<span class="description"></span>
										<?php if(isset($field->message) && $field->message!='') { ?>
											<span class="description"><?php echo $field->message; ?></span>
										<?php } ?>
									</td>
								</tr>
								<script>
									jQuery(".color_spectrum").spectrum({
									    color: "<?php echo $value; ?>",
									    preferredFormat: "hex",
									});
								</script>

							<?php } ?>

							<script type="text/javascript">
		        				jQuery(document).ready(function($) { 
		        			
									<?php
										if($field->showif == 'show') { ?>

											<?php if($field->pcondition == 'not-empty') { ?>
												$("#<?php echo $field->condition_field ?>").change(function(){
													$("#a<?php echo $field->name ?>").show();
												});

												$("#a<?php echo $field->name ?>").show();

												$('input:radio').change(function() {

													if ($(this).is(':checked')) {
														var aa = $(this).val();
											        }

													if(aa != '') {
												 		$("#a<?php echo $field->name ?>").show();
												 	} else {
												 		$("#a<?php echo $field->name ?>").hide();
												 	}
											 	});

											 	var aa = $("input[type='radio']:checked").val();
											 	if(aa == "<?php echo $field->condition_value; ?>") {
											 		$("#a<?php echo $field->name ?>").show();
											 	} else {
											 		$("#a<?php echo $field->name ?>").hide();
											 	}


											<?php } else if($field->pcondition == 'equal-to') { ?>
												$("#<?php echo $field->condition_field ?>").change(function(){
												 	if($("#<?php echo $field->condition_field ?>").val() == "<?php echo $field->condition_value; ?>") {
												 		$("#a<?php echo $field->name ?>").show();
												 	} else {
												 		$("#a<?php echo $field->name ?>").hide();
												 	}
												});

												if($("#<?php echo $field->condition_field ?>").val() == "<?php echo $field->condition_value; ?>") {
											 		$("#a<?php echo $field->name ?>").show();
											 	} else {
											 		$("#a<?php echo $field->name ?>").hide();
											 	}

												$('input:radio').change(function() {

													if ($(this).is(':checked')) {
														var aa = $(this).val();
											        }

													if(aa == "<?php echo $field->condition_value; ?>") {
												 		$("#a<?php echo $field->name ?>").show();
												 	} else {
												 		$("#a<?php echo $field->name ?>").hide();
												 	}
											 	});

											 	var aa = $("input[type='radio']:checked").val();
											 	if(aa == "<?php echo $field->condition_value; ?>") {
											 		$("#a<?php echo $field->name ?>").show();
											 	} else {
											 		$("#a<?php echo $field->name ?>").hide();
											 	}

										
											<?php } else if($field->pcondition == 'not-equal-to') { ?>

												$("#<?php echo $field->condition_field ?>").change(function(){
												 	if($("#<?php echo $field->condition_field ?>").val() != "<?php echo $field->condition_value; ?>") {
												 		$("#a<?php echo $field->name ?>").show();
												 	} else {
												 		$("#a<?php echo $field->name ?>").hide();
												 	}
												});

												if($("#<?php echo $field->condition_field ?>").val() != "<?php echo $field->condition_value; ?>") {
											 		$("#a<?php echo $field->name ?>").show();
											 	} else {
											 		$("#a<?php echo $field->name ?>").hide();
											 	}

												$('input:radio').change(function() {

													if ($(this).is(':checked')) {
														var aa = $(this).val();
											        }

													if(aa != "<?php echo $field->condition_value; ?>") {
												 		$("#a<?php echo $field->name ?>").show();
												 	} else {
												 		$("#a<?php echo $field->name ?>").hide();
												 	}
											 	});


											 	var aa = $("input[type='radio']:checked").val();
											 	if(aa != "<?php echo $field->condition_value; ?>") {
											 		$("#a<?php echo $field->name ?>").show();
											 	} else {
											 		$("#a<?php echo $field->name ?>").hide();
											 	}


											<?php } else if($field->pcondition == 'checked') { ?>

												$("#c<?php echo $field->condition_field ?>").change(function(){ 

													if ($(this).is(':checked')) {
											           $("#a<?php echo $field->name ?>").show();
											        } else {
											        	$("#a<?php echo $field->name ?>").hide();
											        }
												});

												if ($("#c<?php echo $field->condition_field ?>").is(':checked')) { 
													$("#a<?php echo $field->name ?>").show();
												} else {
													$("#a<?php echo $field->name ?>").hide();
												}

											<?php } ?>
						        			
						        		<?php } elseif($field->showif == 'hide') { ?>

						        			<?php if($field->pcondition == 'not-empty') { ?>
												$("#<?php echo $field->condition_field ?>").change(function(){
													$("#a<?php echo $field->name ?>").hide();
												});

												$("#a<?php echo $field->name ?>").hide();

												
												$('input:radio').change(function() {

													if ($(this).is(':checked')) {
														var aa = $(this).val();
											        }

													if(aa != '') {
												 		$("#a<?php echo $field->name ?>").hide();
												 	} else {
												 		$("#a<?php echo $field->name ?>").show();
												 	}
											 	});


											 	var aa = $("input[type='radio']:checked").val();
											 	if(aa == "<?php echo $field->condition_value; ?>") {
											 		$("#a<?php echo $field->name ?>").hide();
											 	} else {
											 		$("#a<?php echo $field->name ?>").show();
											 	}


											<?php } else if($field->pcondition == 'equal-to') { ?>
												$("#<?php echo $field->condition_field ?>").change(function(){
												 	if($("#<?php echo $field->condition_field ?>").val() == "<?php echo $field->condition_value; ?>") {
												 		$("#a<?php echo $field->name ?>").hide();
												 	} else {
												 		$("#a<?php echo $field->name ?>").show();
												 	}
												});

												if($("#<?php echo $field->condition_field ?>").val() == "<?php echo $field->condition_value; ?>") {
											 		$("#a<?php echo $field->name ?>").hide();
											 	} else {
											 		$("#a<?php echo $field->name ?>").show();
											 	}

												$('input:radio').change(function() {

													if ($(this).is(':checked')) {
														var aa = $(this).val();
											        }


													if(aa == "<?php echo $field->condition_value; ?>") {
												 		$("#a<?php echo $field->name ?>").hide();
												 	} else {
												 		$("#a<?php echo $field->name ?>").show();
												 	}
											 	});

											 	var aa = $("input[type='radio']:checked").val();
											 	if(aa == "<?php echo $field->condition_value; ?>") {
											 		$("#a<?php echo $field->name ?>").hide();
											 	} else {
											 		$("#a<?php echo $field->name ?>").show();
											 	}
											
											<?php } else if($field->pcondition == 'not-equal-to') { ?>

												$("#<?php echo $field->condition_field ?>").change(function(){
												 	if($("#<?php echo $field->condition_field ?>").val() != "<?php echo $field->condition_value; ?>") {
												 		$("#a<?php echo $field->name ?>").hide();
												 	} else {
												 		$("#a<?php echo $field->name ?>").show();
												 	}
												});

												if($("#<?php echo $field->condition_field ?>").val() != "<?php echo $field->condition_value; ?>") {
											 		$("#a<?php echo $field->name ?>").hide();
											 	} else {
											 		$("#a<?php echo $field->name ?>").show();
											 	}

												$('input:radio').change(function() {

													if ($(this).is(':checked')) {
														var aa = $(this).val();
											        }


													if(aa != "<?php echo $field->condition_value; ?>") {
												 		$("#a<?php echo $field->name ?>").hide();
												 	} else {
												 		$("#a<?php echo $field->name ?>").show();
												 	}
											 	});

											 	var aa = $("input[type='radio']:checked").val();
											 	if(aa != "<?php echo $field->condition_value; ?>") {
											 		$("#a<?php echo $field->name ?>").hide();
											 	} else {
											 		$("#a<?php echo $field->name ?>").show();
											 	}
												
											<?php } else if($field->pcondition == 'checked') { ?>

												$("#c<?php echo $field->condition_field ?>").change(function(){ 

													if ($(this).is(':checked')) {
											           $("#a<?php echo $field->name ?>").hide();
											        } else {
											        	$("#a<?php echo $field->name ?>").show();
											        }
												});


												if ($("#c<?php echo $field->condition_field ?>").is(':checked')) { 
													$("#a<?php echo $field->name ?>").hide();
												} else {
													$("#a<?php echo $field->name ?>").show();
												}

											<?php } ?>
						        			
						        		<?php } else { ?>
						        			
						        		<?php } ?>
							
								});
		        			</script>
                        <?php }
                    } ?>
            </tbody>
        </table>
        
        <?php
    }
    
    //validated all custom fields and save them in table
    function phlox_admin_update_profile_fields($user_id){
        $fields = self::get_all_record();
    	foreach ($fields as $field) { 

    		if ( isset( $_POST[$field->name] ) || isset( $_FILES[$field->name] ) ) {

                //validating file type fields
        		if($field->type == 'file') {
        			if($_FILES[$field->name]['name']!='') { 
						$filecheck = basename($_FILES[$field->name]['name']);
						$ext = strtolower(substr($filecheck, strrpos($filecheck, '.') + 1));


						if($_FILES[$field->name]["size"] > 5000000) {
							echo '<div class="error notice notice-success is-dismissible">';
							echo $field->name.'_error', __( $field->label.': Your file size is bigger than allowed size!', 'woocommerce' );
							echo '</div>';
							return false;

						} else if(!($ext == "jpg" || $ext == "gif" || $ext == "png" || $ext == "JPG" || $ext == "GIF" || $ext == "PNG" || $ext == "jpeg" || $ext == "JPEG"|| $ext == "PDF"|| $ext == "pdf")) {
							echo '<div class="error notice notice-success is-dismissible">';
							echo $field->name.'_error', __( $field->label.': File Type not allowed!', 'woocommerce' );
							echo '</div>';
							return false;
							
						} else {

							$file = time('m').$_FILES[$field->name]['name'];
							$target_path = PLUGIN_PATH.'uploaded_img/';
							$target_path = $target_path . $file;
							$temp = move_uploaded_file($_FILES[$field->name]['tmp_name'], $target_path);
							update_user_meta($user_id,$field->name, PLUGIN_URL.'uploaded_img/'.$file);
						}
			 		} else {
						$file = $_POST['curr_'.$field->name];
						update_user_meta($user_id,$field->name, $file);
					}

                //validating multiselect fields
        		} else if($field->type == 'multiselect') { 
        			$prefix = '';
        			$multi = '';
        			foreach ($_POST[$field->name] as $value) {
        				$multi .= $prefix.$value;
						$prefix = ', ';
        			}
                    //updating fields data
        			update_user_meta( $user_id, $field->name, $multi );

        		} else {
                    //updating fields data
					update_user_meta( $user_id, $field->name, sanitize_text_field( $_POST[$field->name] ) );
				}

    		}
    	}
    }
}
