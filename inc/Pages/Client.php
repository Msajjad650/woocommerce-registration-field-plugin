<?php 
/**
 * @package PhloxPlugin
 */


namespace Inc\Pages;
use WC_Countries;
use WP_Error;

//use Inc\Init;
session_start();

//including fb twitter autoload files.
include (PLUGIN_PATH . 'facebook-sdk/vendor/autoload.php');
require PLUGIN_PATH. 'twitter-sdk/twitteroauth.php';

/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/

class Client{

	public function register(){
		add_action('wp_loaded', array($this, 'phlox_include_scripts'));
        add_action('woocommerce_register_form_start', array($this, 'phlox_wooc_register_fields_start'));
        add_action('wp_ajax_phlox_get_states', array($this, 'phlox_get_states'));
		add_action('wp_ajax_nopriv_phlox_get_states', array($this, 'phlox_get_states'));
		add_action('woocommerce_register_form', array($this, 'phlox_wooc_register_fields_end'));
		add_action('woocommerce_register_post', array($this, 'phlox_wooc_validate_register_fields'), 10, 3);
		add_action('woocommerce_created_customer', array($this, 'phlox_wooc_save_register_fields'));

		add_filter('query_vars', array($this, 'add_query_vars'), 0);
		add_action('init', array($this, 'add_endpoints'));
		add_action('init', array($this, 'phlox_twitter_loader'), 1);

		add_filter('woocommerce_account_menu_items', array($this, 'new_menu_items'));
		add_action('woocommerce_account_edit-profile_endpoint', array($this, 'endpoint_content'));

		add_action('wp_ajax_phlox_update_all_data', array($this, 'phlox_update_all_data'));
		add_action('wp_ajax_nopriv_phlox_update_all_data', array($this, 'phlox_update_all_data'));

		add_action('woocommerce_checkout_init', array($this, 'phlox_show_billing_fields'), 10, 1);
		add_action('woocommerce_checkout_process', array($this, 'validate_fields_checkout'));

		add_action( 'woocommerce_login_form_end', array($this, 'phlox_front_login_form_end' ));
		add_action( 'wp_ajax_phlox_facebook_login', array($this, 'phlox_fb_call_back'));
   		add_action( 'wp_ajax_nopriv_phlox_facebook_login', array($this, 'phlox_fb_call_back'));
	}
	
    //including all css and js files
	function phlox_include_scripts(){
		wp_enqueue_script('phlox-ui-script', '//code.jquery.com/ui/1.11.4/jquery-ui.js', array('jquery'), false );
    	wp_enqueue_script( 'phlox-timepicker-js', PLUGIN_URL.'/assets/timepicker.js');
    	wp_enqueue_style('phloc-timepicker-css', PLUGIN_URL.'/assets//timepicker.css');
    	wp_enqueue_style('phloc-datepicker-css', '//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css');
    	
    	wp_enqueue_style('phlox-front-css', PLUGIN_URL.'/assets/style.css');
    	wp_enqueue_script('phlox_custom_js', PLUGIN_URL.'assets/script.js', array('jquery'), false);

    	wp_enqueue_script('Phlox Google reCaptcha JS', '//www.google.com/recaptcha/api.js');

    	wp_enqueue_script('phlox_color_spectrum_js', PLUGIN_URL.'assets/color_spectrum.js', array('jquery'), false);
    	wp_enqueue_style( 'phlox_color_spectrum_css', PLUGIN_URL.'assets/color_spectrum.css');
	}
	
    //handling twitter login
	public function phlox_twitter_loader() {	
		if (isset($_GET['ptwitter']) && $_GET['ptwitter']=='phlogin') { 
			self::twitter_login();
		}else if(isset($_REQUEST['oauth_verifier'])) {
			self::twitter_callback();
		}else if(isset($_REQUEST['code']) && !isset($_REQUEST['action'])){
		    self::phlox_google_plus_login();
		}
	}
	
    //twitter login
	public function twitter_login() {
        
        //get twitter credentials
        $twitter_settings = self::phlox_get_select_options('twitter_settings');
        $twitter_settings = json_decode( $twitter_settings[0]->meta_data, true );
        
        $consumerKey = $twitter_settings['twitter_consumer_key'];
		$consumerSecret = $twitter_settings['twitter_consumer_secret'];
		$callbackUrl = $twitter_settings['twitter_callback_url'];
        
        //validate twitter credentials
    	$connection = new \TwitterOAuth($consumerKey, $consumerSecret);
    	$request_token = $connection->getRequestToken($callbackUrl);
    	
    	//Received token info from twitter
    	$_SESSION['oauth_token'] 			= $request_token['oauth_token'];
    	$_SESSION['oauth_token_secret'] 	= $request_token['oauth_token_secret'];
    	//Any value other than 200 is failure, so continue only if http code is 200
    	if($connection->http_code == '200')
    	{
    		//redirect user to twitter
    		$twitter_url = $connection->getAuthorizeURL($request_token['oauth_token']);
    		
    		header('Location: ' . $twitter_url); 
    	}else{
    		die("error connecting to twitter! try again later!");
    	}
			
		
		
	}
	
    //authorization of credentials and redirection to callback url
	public function twitter_callback() {
	    
	    $twitter_settings = self::phlox_get_select_options('twitter_settings');
        $twitter_settings = json_decode( $twitter_settings[0]->meta_data, true );
        
        $consumerKey = $twitter_settings['twitter_consumer_key'];
		$consumerSecret = $twitter_settings['twitter_consumer_secret'];
	    //$callbackUrl = $twitter_settings['twitter_callback_url'];
	    
		$request_token = [];
    	$ac_token = $_SESSION['oauth_token'];
    	$ac_token_secret = $_SESSION['oauth_token_secret'];
    	$connection = new \TwitterOAuth($consumerKey, $consumerSecret, $ac_token, $ac_token_secret);
    	$access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);
    	$_SESSION['access_token'] = $access_token;
    	
    	if($connection->http_code == '200')
    	{
    		//Redirect user to twitter
    		$_SESSION['status'] = 'verified';
    		$_SESSION['request_vars'] = $access_token;
    		
    		//Insert user into the database
    		$user = $connection->get("account/verify_credentials",['include_email' => 'true']);
    		//print_r($user_info);exit;
    		$wpuserarray = array(
					"user_login" => $user->screen_name,
					"user_email" => $user->email,
					"first_name" => $user->name,
					"description" => $user->description,
					"user_url" => $user->url,
					"nickname" => $user->screen_name,
				);
				
			$userid = self::twitter_create_user($wpuserarray);
			//print_r($userid);
			self::twitter_wplogin($wpuserarray['user_email']);
    	}else{
    		die("error, try again later!");
    	}
		
    }
    
    //create user
    private function twitter_create_user($array) {		
		
		if (!username_exists($array['user_email'])) {
			$userid = wp_insert_user($array );
			return $userid;			
		}else{
			return 'exists';
		}
	}
	
    //set user for login and creating session
	private function twitter_wplogin($username) {
		$user = get_user_by('email' ,$username);
		if($user) {
			$userid = $user->ID;
			wp_set_current_user($userid, $username);
			wp_set_auth_cookie($userid);
			do_action('wp_login', $username, $user);
		}
	}

    //creating menu in woocommerce dashboard
	public function add_query_vars($vars) { 
		$vars[] = 'edit-profile';
		return $vars;
	}

    //creating menu in woocommerce dashboard
	public function add_endpoints() {
		add_rewrite_endpoint( 'edit-profile', EP_ROOT | EP_PAGES );
		flush_rewrite_rules();
	}

    //creating menu in woocommerce dashboard
	public function new_menu_items($items) {
		
		$logout = $items['customer-logout'];
		unset( $items['customer-logout'] );
		
		$items[ 'edit-profile' ] = __( 'Profile Fields', 'woocommerce' );
		
		$items['customer-logout'] = $logout;
		return $items;
	}

    //showing fb twitter google plus icons on login page
	function phlox_front_login_form_end() { 
        
        if(!session_id()) {
            session_start();
        }
        
        if(is_user_logged_in()) { return; }
        
        //get fb settings
        $fb_settings = self::phlox_get_select_options('fb_settings');
        $fb_settings = json_decode( $fb_settings[0]->meta_data, true );
    
        //show fb icon if fb setting enabled
	    if($fb_settings['fb_status'] == 'enabled') {

	        $html = '<div id="eo-facebook-wrapper">';
            
	        if(isset($_SESSION['eo_facebook_message'])) {
	            $message = $_SESSION['alka_facebook_message'];
	            $html .= '<div id="eo-facebook-message" class="alert alert-danger">'.$message.'</div>';
	            unset($_SESSION['eo_facebook_message']);
	        }
	        if($fb_settings['fb_app'] == '' || $fb_settings['fb_secret'] == ''){
                $html .= '<a href="#"><img src="'.PLUGIN_URL.'images/fb1.png" /></a>';
    	        $html .= '</div>';
	        }else{

    	        $html .= '<a href="'.self::getLoginUrl().'"><img src="'.PLUGIN_URL.'images/fb1.png" /></a>';
    	        $html .= '</div>';
	        }
            
	        echo $html;
    	}    	
        
        //get twitter credentials
        $twitter_settings = self::phlox_get_select_options('twitter_settings');
        $twitter_settings = json_decode( $twitter_settings[0]->meta_data, true );
        if($twitter_settings['twitter_status'] == 'enabled') {
            //if enabled show twitter login icon
            echo self::phlox_get_twitter_login_link();
        }
        
        //get google plus settings
        $gplus_settings = self::phlox_get_select_options('gplus_settings');
        $gplus_settings = json_decode( $gplus_settings[0]->meta_data, true );
        if($gplus_settings['gplus_status'] == 'enabled') {
            //if enalbe show google login icon
            self::phlox_google_plus_login();
        }
	}
	
    //show google login icon
	function phlox_google_plus_login(){
        $gplus_settings = self::phlox_get_select_options('gplus_settings');
        $gplus_settings = json_decode( $gplus_settings[0]->meta_data, true );
	    $CLIENT_ID = $gplus_settings['gplus_client_id'];
        $CLIENT_SECRET = $gplus_settings['gplus_client_secret'];
        $REDIRECT_URI = $gplus_settings['gplus_login_url'];
        
        if (isset($_GET['code'])) {
            //get google plus access token using twitter credntials
            $data = self::phlox_google_get_accessToken($CLIENT_ID, $REDIRECT_URI, $CLIENT_SECRET, $_GET['code']);
		
    		// Get user information
    		if(isset($data['access_token'])){
                //get user information
        		$user_info = self::phlox_google_get_user_info($data['access_token']);
                
                $wpuserarray = array(
    					"user_login" => $user_info['name']['familyName'],
    					"user_pass" => $user_info['id'],
    					"user_email" => $user_info['emails'][0]['value'],
    					"name" => $user_info['displayName'],
    					"image_url" => $user_info['image']['url'],
    					"profile_url" => $user_info['url'],
    					"user_nicename" => $user_info['name']['familyName'],
    					"display_name" => $user_info['name']['familyName']
    				);
    			//creating user
    			$userid = self::twitter_create_user($wpuserarray);
    			//setting user login adn creating session
    			 self::twitter_wplogin($wpuserarray['user_email']);
    			
    		}else{
                // return the login url 
    		    $perm_urls = 'https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/plus.me';
                echo '<a href="https://accounts.google.com/o/oauth2/v2/auth?scope=' . urlencode($perm_urls) . '&redirect_uri=' . urlencode($REDIRECT_URI) . '&response_type=code&client_id=' . $CLIENT_ID . '&access_type=online"><img src="'.PLUGIN_URL.'images/signin_button.png" style="width: 53%; margin-top: 10px; height: 30px;"/></a>';
    		}
			
        } else {
            // get the login url  
            $perm_urls = 'https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/plus.me';
            echo '<a href="https://accounts.google.com/o/oauth2/v2/auth?scope=' . urlencode($perm_urls) . '&redirect_uri=' . urlencode($REDIRECT_URI) . '&response_type=code&client_id=' . $CLIENT_ID . '&access_type=online"><img src="'.PLUGIN_URL.'images/signin_button.png" style="width: 53%; margin-top: 10px; height: 30px;"/></a>';
        }
	}
	
    //get google plus access token
	function phlox_google_get_accessToken($client_id, $redirect_uri, $client_secret, $code) {	
		$url = 'https://accounts.google.com/o/oauth2/token';			
		
		$curlPost = 'client_id=' . $client_id . '&redirect_uri=' . $redirect_uri . '&client_secret=' . $client_secret . '&code='. $code . '&grant_type=authorization_code';
		$ch = curl_init();		
		curl_setopt($ch, CURLOPT_URL, $url);		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
		curl_setopt($ch, CURLOPT_POST, 1);		
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);	
		$data = json_decode(curl_exec($ch), true);
		$http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);		
			
		return $data;
	}
	
    //get google plus user info
	function phlox_google_get_user_info($access_token){
	    $url = 'https://www.googleapis.com/plus/v1/people/me';			
		
		$ch = curl_init();		
		curl_setopt($ch, CURLOPT_URL, $url);		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '. $access_token));
		$data = json_decode(curl_exec($ch), true);
		$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);		
		if($http_code != 200) 
			throw new Exception('Error : Failed to get user information');
			
		return $data;
	}

    //return login url for facebook
	function getLoginUrl() {
        if(!session_id()) {
            session_start();
        }

        //initializing fb
        $fb = self::FbApiCall();
        $helper = $fb->getRedirectLoginHelper();
        $permissions = ['email'];
        $callbackurl = admin_url('admin-ajax.php').'?action=phlox_facebook_login';
        $url = $helper->getLoginUrl($callbackurl, $permissions);
        return esc_url($url);

    }

    //initializing fb
    function FbApiCall() {
    	$fb_settings = self::phlox_get_select_options('fb_settings');
        $fb_settings = json_decode( $fb_settings[0]->meta_data, true );

		$app_id = $fb_settings['fb_app'];
		$app_secret = $fb_settings['fb_secret'];

        $facebook = new \Facebook\Facebook([
            'app_id' => $app_id,
            'app_secret' => $app_secret,
            'default_graph_version' => 'v2.2',
            'persistent_data_handler' => 'session'
        ]);

        return $facebook;
    }

    //return twitter login url
    function phlox_get_twitter_login_link() {
		$authUrl = get_permalink( get_option('woocommerce_myaccount_page_id') ).'?ptwitter=phlogin&authenticate=1';
		return '<a href="'.filter_var($authUrl, FILTER_SANITIZE_URL).'"><img style="margin-top:10px;" src="'.PLUGIN_URL.'images/tw.png"/></a>';
	}

    //when clicked on fb login icon
	function phlox_fb_call_back() {
	    
    	if(!session_id()) {
            session_start();
        }

        //get redirect url
        $this->redirect_url = get_permalink( get_option('woocommerce_myaccount_page_id') );
        //fb initialization
        $fb = $this->FbApiCall();
        //access token
        $this->access_token = $this->getToken($fb);
        //get user info
        $this->facebook_details = $this->getUserDetails($fb);
        //user creation and login
        $this->loginUser();
        $this->createUser();
        header("Location: ".$this->redirect_url, true);
        die();
	}

    //return access token fb
	function getToken($fb) {
	    
        $_SESSION['FBRLH_state'] = $_GET['state'];

        $helper = $fb->getRedirectLoginHelper();
        try {
            $accessToken = $helper->getAccessToken();
        }
        catch(Facebook\Exceptions\FacebookResponseException $e) {
            $error = __('Graph returned an error: ','woocommerce'). $e->getMessage();
            $message = array(
                'type' => 'error',
                'content' => $error
            );
        }

        if (!isset($accessToken)) {
            $_SESSION['alka_facebook_message'] = $message;
            header("Location: ".$this->redirect_url, true);
            die();
        }

        return $accessToken->getValue();

    }

    //get fb user info
    function getUserDetails($fb){

        try {
            $response = $fb->get('/me?fields=id,name,first_name,last_name,email,link', $this->access_token);
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            $message = __('Graph returned an error: ','woocommerce'). $e->getMessage();
            $message = array(
                'type' => 'error',
                'content' => $error
            );
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            $message = __('Facebook SDK returned an error: ','woocommerce'). $e->getMessage();
            $message = array(
                'type' => 'error',
                'content' => $error
            );
        }

        // If we caught an error
        if (isset($message)) {
            $_SESSION['eo_facebook_message'] = $message;
            header("Location: ".$this->redirect_url, true);
            die();
        }

        return $response->getGraphUser();

    }

    //creating session and login fb user 
    function loginUser() {

        $wp_users = get_users(array(
            'meta_key'     => 'phlox_facebook_id',
            'meta_value'   => $this->facebook_details['id'],
            'number'       => 1,
            'count_total'  => false,
            'fields'       => 'id',
        ));

        if(empty($wp_users[0])) {
            return false;
        }

        wp_set_auth_cookie( $wp_users[0] );

    }

    //create user and update information
    function createUser() {
    
        $fb_user = $this->facebook_details;

        if (!username_exists($fb_user['email'])) {
            $username = sanitize_user(str_replace(' ', '_', strtolower($this->facebook_details['name'])));    
            $new_user = wp_create_user($username, wp_generate_password(), $fb_user['email']);    
            if(is_wp_error($new_user)) {
                $_SESSION['eo_facebook_message'] = $new_user->get_error_message();
                header("Location: ".$this->redirect_url, true);
                die();
            }
        }
        update_user_meta( $new_user, 'first_name', $fb_user['first_name'] );
        update_user_meta( $new_user, 'last_name', $fb_user['last_name'] );
        update_user_meta( $new_user, 'user_url', $fb_user['link'] );
        update_user_meta( $new_user, 'phlox_facebook_id', $fb_user['id'] );

        wp_set_auth_cookie( $new_user );

    }

    //print custom fields on registration page
	function phlox_wooc_register_fields_start() { 
        //show heading for fields
		echo self::get_title_heading();
        //get all defaults fields
		$default_fields = self::phlox_get_default_fields();

		foreach($default_fields as $def_fields){
            //show enabled text fields
			if($def_fields->type == 'text' && $def_fields->status == 'enabled') { ?>
				<p class="form-row <?php echo $def_fields->width; ?>">
					<label for="<?php echo $def_fields->name; ?>"><?php _e($def_fields->label, 'woocommerce'); ?> 
						<?php if($def_fields->is_required == 1) { ?> <span class="required">*</span> <?php } ?>
					</label>
					<input type="text" class="input-text" name="<?php echo $def_fields->name; ?>" value="<?php if ( ! empty( $_POST[$def_fields->name] ) ) esc_attr_e( $_POST[$def_fields->name] ); ?>" placeholder="<?php echo $def_fields->placeholder; ?>" />
					<?php if(isset($def_fields->message) && $def_fields->message!='') { ?>
						<span style="width:100%;float: left"><?php echo $def_fields->message; ?></span>
					<?php } ?>
				</p>
                <!-- show enabled phone fields -->
			<?php } elseif($def_fields->type == 'phn' && $def_fields->status == 'enabled') { ?>
				<p class="form-row <?php echo $def_fields->width; ?>">
					<label for="<?php echo $def_fields->name; ?>"><?php _e($def_fields->label, 'woocommerce'); ?> 
						<?php if($def_fields->is_required == 1) { ?> <span class="required">*</span> <?php } ?>
					</label>
					<input type="number" class="input-text" name="<?php echo $def_fields->name; ?>" value="<?php if ( ! empty( $_POST[$def_fields->name] ) ) esc_attr_e( $_POST[$def_fields->name] ); ?>" placeholder="<?php echo $def_fields->placeholder; ?>" />
					<?php if(isset($def_fields->message) && $def_fields->message!='') { ?>
						<span style="width:100%;float: left"><?php echo $def_fields->message; ?></span>
					<?php } ?>
				</p>
			
            <!-- show enabled select fields -->
			<?php } else if($def_fields->type == 'select' && $def_fields->status == 'enabled') { ?>

    			<?php if($def_fields->name == 'billing_country') { 
    			    global $woocommerce;
    			    $countries_obj   = new WC_Countries();
                    $countries   = $countries_obj->__get('countries');
					if ( ! empty( $_POST[$def_fields->name] ) ) {
						$bcountry = $_POST[$def_fields->name]; 
					} else {
						$bcountry = '';
					} ?>
        			<p class="form-row <?php echo $def_fields->width; ?>">
						<label for="<?php echo $def_fields->name; ?>"><?php _e($def_fields->label, 'woocommerce'); ?> 
							<?php if($def_fields->is_required == 1) { ?> <span class="required">*</span> <?php } ?>
						</label>
						<select class="js-example-basic-single" name="<?php echo $def_fields->name; ?>" onchange="selectState(this.value);">
							<option value=""><?php echo _e('Select a country...', 'woocommerce'); ?></option>
							<?php foreach($countries as $key => $value) { ?>
								<option value="<?php echo $key; ?>" <?php echo selected($bcountry,$key); ?>><?php echo $value; ?></option>
							<?php } ?>
						</select>
						
						<?php if(isset($def_fields->message) && $def_fields->message!='') { ?>
							<span style="width:100%;float: left"><?php echo $def_fields->message; ?></span>
						<?php } ?>
					</p>

				<?php } if($def_fields->name == 'billing_state') { ?>

					<p id="statedrop" class="form-row <?php echo $def_fields->width; ?>">
						<label for="<?php echo $def_fields->name; ?>"><?php _e( $def_fields->label, 'woocommerce' ); ?> 
							<?php if($def_fields->is_required == 1) { ?> <span class="required">*</span> <?php } ?>
						</label>

						<input type="text" class="input-text" name="<?php echo $def_fields->name; ?>" id="statedrop" placeholder="<?php echo $def_fields->placeholder; ?>" />
						
						<?php if(isset($def_fields->message) && $def_fields->message!='') { ?>
							<span style="width:100%;float: left"><?php echo $def_fields->message; ?></span>
						<?php } ?>
					</p>

				<?php } ?>

				<script type="text/javascript">
					jQuery(document).ready(function() {
					    //jQuery('.js-example-basic-single').select2();
					});

					jQuery(document).ready(function() {

						<?php if($def_fields->name == 'billing_country') { ?>


							<?php if(isset($_POST[$def_fields->name]) && $_POST[$def_fields->name]!='') { ?>
								var country = "<?php echo $_POST[$def_fields->name]; ?>";
							<?php } else { ?>
								var country = "";
							<?php } ?>

							<?php if(isset($_POST[$def_fields->name]) && $_POST[$def_fields->name]!='') { ?>
								var se_state = "<?php echo $_POST['billing_state']; ?>";
							<?php } else { ?>
								var se_state = "";
							<?php } ?>							

							var ajaxurl = "<?php echo admin_url( 'admin-ajax.php'); ?>";
							var fname = "<?php echo $def_fields->name; ?>";
							var flabel = "<?php echo $def_fields->label; ?>";
							var fmessage = "<?php echo $def_fields->message; ?>";
							var required = "<?php echo $def_fields->is_required; ?>";
							var width = "<?php echo $def_fields->width; ?>";

							jQuery.ajax({
								type: 'POST',   
								url: ajaxurl,
								data: {"action": "phlox_get_states","country":country,"fname":fname,"flabel":flabel,"fmessage":fmessage,"required":required,"width":width,"se_state":se_state}, 
								success: function(data){ 
									jQuery('#statedrop').html(data);
								}
							});


						<?php } ?>
					});

					function selectState(country) { 
						var ajaxurl = "<?php echo admin_url( 'admin-ajax.php'); ?>";
						var fname = "<?php echo $def_fields->name; ?>";
						var flabel = "<?php echo $def_fields->label; ?>";
						var fmessage = "<?php echo $def_fields->message; ?>";
						var required = "<?php echo $def_fields->is_required; ?>";
						var width = "<?php echo $def_fields->width; ?>";

						jQuery.ajax({
							type: 'POST',   
							url: ajaxurl, 
							data: {"action": "phlox_get_states","country":country,"fname":fname,"flabel":flabel,"fmessage":fmessage,"required":required,"width":width}, 
							success: function(data){ 
								jQuery('#statedrop').html(data);
							}
						});  
					}

				</script>

		<?php }

        }
	}

    //return fields heading
	function get_title_heading(){
		global $wpdb;
        $table_name = $wpdb->prefix.'phlox_meta_settings';
        $getfields = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `meta_name` = 'general_settings'",""));
        $general_settings = json_decode($getfields[0]->meta_data, true);
        if($general_settings['account_title'] != ''){
        	return '<h3>'.$general_settings['account_title'].'</h3>';
        }else{
        	return '';
        }
	}

    //return profile title
	function get_profile_title(){
		global $wpdb;
        $table_name = $wpdb->prefix.'phlox_meta_settings';
        $getfields = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `meta_name` = 'general_settings'",""));
        $general_settings = json_decode($getfields[0]->meta_data, true);
        if($general_settings['profile_title'] != ''){
        	return '<h3>'.$general_settings['profile_title'].'</h3>';
        }else{
        	return '';
        }
	}

    //return all defaults fields
	function phlox_get_default_fields(){
		global $wpdb;
        $table_name = $wpdb->prefix.'phlox_reg_default_fields';
        return $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name",""));
	}

    //return all fields
	function phlox_get_fields(){
		global $wpdb;
        $table_name = $wpdb->prefix.'phlox_reg_fields';
        return $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name ORDER BY sort_order ASC",""));
	}

    //return all states on the basis of country
	function phlox_get_states(){
		$country = $_POST['country'];
		$width = $_POST['width'];
		$fname = $_POST['fname'];
		$flabel = $_POST['flabel'];
		$fmessage = $_POST['fmessage'];
		$required = $_POST['required'];
		@$se_state = $_POST['se_state'];


		$countries_obj   = new WC_Countries();
		$states = $countries_obj->get_states($country);
		
        //populating all states
		if(!empty($states)) { ?>

			<p id="statedrop" class="form-row <?php echo $width; ?>">
				<label for="<?php echo $fname; ?>"><?php _e( $flabel, 'woocommerce' ); ?> 
					<?php if($required == 1) { ?> <span class="required">*</span> <?php } ?>
				</label>

				<select class="js-example-basic-single" name="billing_state">
					<option value=""><?php echo _e('Select a county / state...', 'woocommerce'); ?></option>
					
					<?php foreach($states as $key => $value) { ?>
						<option value="<?php echo $key; ?>" <?php echo selected($se_state, $key); ?>><?php echo $value; ?></option>
					<?php } ?>
				</select>

				<?php if(isset($fmessage) && $fmessage!='') { ?>
					<span style="width:100%;float: left"><?php echo $fmessage; ?></span>
				<?php } ?>
			</p>

		

		<?php } else { ?>
			<p id="statedrop" class="form-row <?php echo $width; ?>">
				<input type="hidden" name="billing_state" value="<?php echo $country; ?>" />
			</p>

		<?php } ?>

		<script type="text/javascript">
			/*jQuery(document).ready(function() {
			    jQuery('.js-example-basic-single').select2();
			});	*/			

		</script>


		<?php die();
	}

    //return metadata of fields
	function phlox_get_select_options($field_id){
		global $wpdb;
        $table_name = $wpdb->prefix.'phlox_meta_settings';
        return $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `meta_name` = '$field_id'",""));
	}

    //return google captcha settings
	function get_recaptcha_site_key(){
		global $wpdb;
        $table_name = $wpdb->prefix.'phlox_meta_settings';
        $getfields = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `meta_name` = 'recaptcha_settings'",""));
        $recaptcha_settings = json_decode($getfields[0]->meta_data, true);
        if($recaptcha_settings['recaptcha_site'] != ''){
        	return $recaptcha_settings['recaptcha_site'];
        }else{
        	return '';
        }
	}

    //return google captcha secret key
	function get_recaptcha_secret_key(){
		global $wpdb;
        $table_name = $wpdb->prefix.'phlox_meta_settings';
        $getfields = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE `meta_name` = 'recaptcha_settings'",""));
        $recaptcha_settings = json_decode($getfields[0]->meta_data, true);
        if($recaptcha_settings['recaptcha_secret'] != ''){
        	return $recaptcha_settings['recaptcha_secret'];
        }else{
        	return '';
        }
	}

    //return all custom fields and heading
	function phlox_wooc_register_fields_end(){ 
        //get profile title
		self::get_profile_title(); ?>
    	<input type="hidden" name="pageID" value="<?php echo get_queried_object_id(); ?>" />
    	<?php 
        //get all custom fields
    	$fields = self::phlox_get_fields();
    	$clss = '';
    	foreach ($fields as $field) { 
    		if($field->showif == 'show') {
    			$clss = 'hide';
    		} elseif($field->showif == 'hide') {
    			$clss = 'show';
    		} else {
    			$clss = '';
    		}

            //show all text fields
    		if($field->type == 'text' && $field->is_hide == 0) { ?>
				
	        	<p id="a<?php echo $field->name; ?>" class="form-row <?php echo $field->width; ?> <?php echo $clss; ?>">
					<label for="<?php echo $field->name; ?>"><?php _e( $field->label, 'woocommerce' ); ?> 
						<?php if($field->is_required == 1) { ?> <span class="required">*</span> <?php } ?>
					</label>
					<input type="text" class="input-text" id="<?php echo $field->name; ?>" name="<?php echo $field->name; ?>" value="<?php if ( ! empty( $_POST[$field->name] ) ) esc_attr_e( $_POST[$field->name] ); ?>" placeholder="<?php echo $field->placeholder; ?>" />
					<?php if(isset($field->message) && $field->message!='') { ?>
						<span style="width:100%;float: left"><?php echo $field->message; ?></span>
					<?php } ?>
				</p>
        	
            <!-- show all textarea fields -->	
        	<?php } else if($field->type == 'textarea' && $field->is_hide == 0) { ?>

	        	<p id="a<?php echo $field->name; ?>" class="form-row <?php echo $field->width; ?>  <?php echo $clss; ?>">
					<label for="<?php echo $field->name; ?>"><?php _e( $field->label, 'woocommerce' ); ?> 
						<?php if($field->is_required == 1) { ?> <span class="required">*</span> <?php } ?>
					</label>
					<textarea name="<?php echo $field->name; ?>" id="<?php echo $field->name; ?>" class="input-text" cols="5" rows="2" placeholder="<?php echo $field->placeholder; ?>"><?php if ( ! empty( $_POST[$field->name] ) ) esc_attr_e( $_POST[$field->name] ); ?></textarea>
					<?php if(isset($field->message) && $field->message!='') { ?>
						<span style="width:100%;float: left"><?php echo $field->message; ?></span>
					<?php } ?>
				</p>

            <!-- show all select fields -->
        	<?php } else if($field->type == 'select' && $field->is_hide == 0) { ?>

	        	<p id="a<?php echo $field->name; ?>" class="form-row <?php echo $field->width; ?>  <?php echo $clss; ?>">
					<label for="<?php echo $field->name; ?>"><?php _e( $field->label, 'woocommerce' ); ?> 
						<?php if($field->is_required == 1) { ?> <span class="required">*</span> <?php } ?>
					</label>
					<select name="<?php echo $field->name; ?>" id="<?php echo $field->name; ?>">
					    <option value="">Please select value</option>
					<?php $option_rec = self::phlox_get_select_options($field->field_id);
						$options = json_decode($option_rec[0]->meta_data, true);
						foreach($options as $key => $value) { ?>

						<?php if ( ! empty( $_POST[$field->name] ) ) { ?>
							<option value="<?php echo $value; ?>" <?php echo selected($_POST[$field->name], $key); ?>><?php echo $key; ?></option>
						<?php } else { ?>
							<option value="<?php echo $value; ?>"><?php echo $key; ?></option>
						<?php } ?>

					<?php } ?>

					</select>
					<?php if(isset($field->message) && $field->message!='') { ?>
						<span style="width:100%;float: left"><?php echo $field->message; ?></span>
					<?php } ?>
					
				</p>

            <!-- show all multiselect fields -->
        	<?php } else if($field->type == 'multiselect' && $field->is_hide == 0) { ?>

	    		<p id="a<?php echo $field->name; ?>" class="form-row <?php echo $field->width; ?>  <?php echo $clss; ?>">
					<label for="<?php echo $field->name; ?>"><?php _e( $field->label, 'woocommerce' ); ?> 
						<?php if($field->is_required == 1) { ?> <span class="required">*</span> <?php } ?>
					</label>
					<select style="height:150px;" multiple="true" name="<?php echo $field->name; ?>[]" id="<?php echo $field->name; ?>">
					
					<?php $option_rec = self::phlox_get_select_options($field->field_id);
							$options = json_decode($option_rec[0]->meta_data, true);
						foreach($options as $key => $value) { ?>
						
						<?php if ( ! empty( $_POST[$field->name] ) ) { ?>
							<option value="<?php echo $value; ?>" <?php if(in_array($key, $_POST[$field->name])) { echo "selected"; } ?>><?php echo $key; ?></option>
						<?php } else { ?>
							<option value="<?php echo $value; ?>"><?php echo $key; ?></option>
						<?php } ?>

					<?php } ?>

					</select>
					<?php if(isset($field->message) && $field->message!='') { ?>
						<span style="width:100%;float: left"><?php echo $field->message; ?></span>
					<?php } ?>
					
				</p>

            <!-- show all checkbox fields -->
        	<?php } else if($field->type == 'checkbox' && $field->is_hide == 0) { ?>

	        	<p id="a<?php echo $field->name; ?>" class="form-row <?php echo $field->width; ?>  <?php echo $clss; ?>">					
					<?php if ( ! empty( $_POST[$field->name] ) ) { ?>
					
						<input type="checkbox" id="c<?php echo $field->fname; ?>" name="<?php echo $field->name; ?>" value="1" class="input-checkbox" <?php echo checked($_POST[$field->name], 1); ?>>
					<?php } else { ?>
						<input type="checkbox" id="c<?php echo $field->name; ?>" name="<?php echo $field->name; ?>" value="1" class="input-checkbox">
					<?php } ?>

					<?php _e( $field->label, 'woocommerce' ); ?> 
						<?php if($field->is_required == 1) { ?> <span class="required">*</span> <?php } ?>
					<?php if(isset($field->message) && $field->message!='') { ?>
						<span style="width:100%;float: left"><?php echo $field->message; ?></span>
					<?php } ?>
				</p>

            <!-- show all checkbox for terms and conditions fields -->
        	<?php } else if($field->type == 'checkboxa') { ?>

	        	<p id="a<?php echo $field->name; ?>" class="form-row <?php echo $field->width; ?>  <?php echo $clss; ?>">					
					<?php if ( ! empty( $_POST[$field->name] ) ) { ?>
					
						<input type="checkbox" id="c<?php echo $field->name; ?>" name="<?php echo $field->name; ?>" value="1" class="input-checkbox" <?php echo checked($_POST[$field->name], 1); ?>>
					<?php } else { ?>
						<input type="checkbox" id="c<?php echo $field->name; ?>" name="<?php echo $field->name; ?>" value="1" class="input-checkbox">
					<?php } ?>
					<?php if($field->is_required == 1) { ?> <span class="required">*</span> <?php } ?>
					<?php _e( $field->checkbox_text, 'woocommerce' ); ?> 
						
					<?php if(isset($field->message) && $field->message!='') { ?>
						<span style="width:100%;float: left"><?php echo $field->message; ?></span>
					<?php } ?>
				</p>

            <!-- show all radio button fields -->
        	<?php } else if($field->type == 'radio' && $field->is_hide == 0) { ?>

	        	<p id="a<?php echo $field->name; ?>" class="form-row <?php echo $field->width; ?>  <?php echo $clss; ?>">
	        		<label for="<?php echo $field->name; ?>"><?php _e( $field->label, 'woocommerce' ); ?> 
						<?php if($field->is_required == 1) { ?> <span class="required">*</span> <?php } ?>
					</label>
					<?php $option_rec = self::phlox_get_select_options($field->field_id);
					    if(isset($option_rec[0]->meta_data)){
						    $options = json_decode($option_rec[0]->meta_data, true);
						    foreach($options as $key => $value) {
					?>

					<?php if ( ! empty( $_POST[$field->name] ) ) { ?>
						<input type="radio" name="<?php echo $field->name;?>" value="<?php echo $value;?>" class="input-checkbox" <?php echo checked($_POST[$field->name], $key); ?>> <?php echo $value; ?>
					<?php } else { ?>
						<input type="radio" name="<?php echo $field->name; ?>" value="<?php echo $value; ?>" class="input-checkbox"> <?php echo $key; ?>
					<?php } ?>
					
					<?php } 
					} ?>
					<?php if(isset($field->message) && $field->message!='') { ?>
						<span style="width:100%;float: left"><?php echo $field->message; ?></span>
					<?php } ?>
	        	</p>

        	<!-- show all datepicker fields -->
            <?php } else if($field->type == 'datepicker' && $field->is_hide == 0) { ?>

	        	<p id="a<?php echo $field->name; ?>" class="form-row <?php echo $field->width; ?>  <?php echo $clss; ?>">
					<label for="<?php echo $field->name; ?>"><?php _e( $field->label, 'woocommerce' ); ?> 
						<?php if($field->is_required == 1) { ?> <span class="required">*</span> <?php } ?>
					</label>
					<input type="text" class="input-text datepick" name="<?php echo $field->name; ?>" id="<?php echo $field->name; ?>" value="<?php if ( ! empty( $_POST[$field->name] ) ) esc_attr_e( $_POST[$field->name] ); ?>" placeholder="<?php echo $field->placeholder; ?>" />
					<?php if(isset($field->message) && $field->message!='') { ?>
						<span style="width:100%;float: left"><?php echo $field->message; ?></span>
					<?php } ?>
				</p>

        	<!-- show all timepicker fields -->
            <?php } else if($field->type == 'timepicker' && $field->is_hide == 0) { ?>

	        	<p id="a<?php echo $field->name; ?>" class="form-row <?php echo $field->width; ?>  <?php echo $clss; ?>">
					<label for="<?php echo $field->name; ?>"><?php _e( $field->label, 'woocommerce' ); ?> 
						<?php if($field->is_required == 1) { ?> <span class="required">*</span> <?php } ?>
					</label>
					<input type="text" class="input-text timepick" name="<?php echo $field->name; ?>" id="<?php echo $field->name; ?>" value="<?php if ( ! empty( $_POST[$field->name] ) ) esc_attr_e( $_POST[$field->name] ); ?>" placeholder="<?php echo $field->placeholder; ?>" />
					<?php if(isset($field->message) && $field->message!='') { ?>
						<span style="width:100%;float: left"><?php echo $field->message; ?></span>
					<?php } ?>
				</p>

        	<!-- show all password fields -->
            <?php } else if($field->type == 'password' && $field->is_hide == 0) { ?>

	        	<p id="a<?php echo $field->name; ?>" class="form-row <?php echo $field->width; ?>  <?php echo $clss; ?>">
					<label for="<?php echo $field->name; ?>"><?php _e( $field->label, 'woocommerce' ); ?> 
						<?php if($field->is_required == 1) { ?> <span class="required">*</span> <?php } ?>
					</label>
					<input type="password" class="input-text" name="<?php echo $field->name; ?>" id="<?php echo $field->name; ?>" value="<?php if ( ! empty( $_POST[$field->name] ) ) esc_attr_e( $_POST[$field->name] ); ?>" placeholder="<?php echo $field->placeholder; ?>" />
					<?php if(isset($field->message) && $field->message!='') { ?>
						<span style="width:100%;float: left"><?php echo $field->message; ?></span>
					<?php } ?>
				</p>

        	<!-- show all file fields -->
            <?php } else if($field->type == 'file' && $field->is_hide == 0) { ?>

	        	<p id="a<?php echo $field->name; ?>" class="form-row <?php echo $field->width; ?>  <?php echo $clss; ?>">
					<label for="<?php echo $field->name; ?>"><?php _e( $field->label, 'woocommerce' ); ?> 
						<?php if($field->is_required == 1) { ?> <span class="required">*</span> <?php } ?>
					</label>
					<input type="file" class="input-text" name="<?php echo $field->name; ?>" id="<?php echo $field->name; ?>" value="" placeholder="<?php echo $field->placeholder; ?>" />
					<?php if(isset($field->message) && $field->message!='') { ?>
						<span style="width:100%;float: left"><?php echo $field->message; ?></span>
					<?php } ?>
					<span style="width:100%; display:none; color:red; float: left" id = "typem<?php echo $field->name; ?>">
						File type not allowed!
					</span>
					<span style="width:100%; display:none; color:red; float: left" id = "sizem<?php echo $field->name; ?>">
						File size exceeded the maximum size!
					</span>
					<img style="display:none;" id="im<?php echo $field->name; ?>" src="#" alt="" width="150" />

					<script type="text/javascript">

						function readURL(input,id) { 
							jQuery('#typem'+id).hide();
							jQuery('#sizem'+id).hide();
							jQuery('#im'+id).attr('src', '');
					        if (input.files && input.files[0]) { 

					        	if(!(input.files[0].type == 'application/pdf' || input.files[0].type == 'image/jpeg' || input.files[0].type == 'image/gif' || input.files[0].type == 'image/png')) {
					        		jQuery('#typem'+id).show();
					        		jQuery("input[name=register]").prop('disabled', true);
					        	} else if(input.files[0].size > 49000000) {

					        		jQuery('#sizem'+id).show();
					        		jQuery("input[name=register]").prop('disabled', true);

					        	} else {

					        		jQuery("input[name=register]").prop('disabled', false);
					        		jQuery('#im'+id).show();
						            var reader = new FileReader();
						            
						            reader.onload = function (e) {
						                jQuery('#im'+id).attr('src', e.target.result);
						            }
						            
						            reader.readAsDataURL(input.files[0]);
					       	 	}
					        }
					    }
					    
					    jQuery("#<?php echo $field->name; ?>").change(function(){
					        readURL(this,'<?php echo $field->name; ?>');
					    });

					</script>
				</p>

        	<!-- show all numeric fields -->
            <?php } elseif($field->type == 'numeric' && $field->is_hide == 0) {  ?>

        		<p id="a<?php echo $field->name; ?>" class="form-row <?php echo $field->width; ?> <?php echo $clss; ?>">
					<label for="<?php echo $field->name; ?>"><?php _e( $field->label, 'woocommerce' ); ?> 
						<?php if($field->is_required == 1) { ?> <span class="required">*</span> <?php } ?>
					</label>
					<input type="number" class="input-text" name="<?php echo $field->name; ?>" id="<?php echo $field->name; ?>" value="<?php if ( ! empty( $_POST[$field->name] ) ) esc_attr_e( $_POST[$field->name] ); ?>" placeholder="<?php echo $field->placeholder; ?>" />
					<?php if(isset($field->message) && $field->message!='') { ?>
						<span style="width:100%;float: left"><?php echo $field->message; ?></span>
					<?php } ?>
				</p>

        	<!-- show google captcha -->
            <?php } else if($field->type == 'captcha') { ?>

        		<p id="a<?php echo $field->name; ?>" class="form-row <?php echo $field->width; ?> <?php echo $clss; ?>">
					<label for="<?php echo $field->name; ?>"><?php _e( $field->label, 'woocommerce' ); ?> 		
						 <span class="required">*</span>
					</label>
                
					<div class="g-recaptcha" data-sitekey="<?php echo self::get_recaptcha_site_key(); ?>"></div>
					<?php if(isset($field->message) && $field->message!='') { ?>
						<span style="width:100%;float: left"><?php echo $field->message; ?></span>
					<?php } ?>			
                </p><p class="form-row full"></p>

        	<!-- show all color fields -->
            <?php } else if($field->type == 'color' && $field->is_hide == 0) { ?>
        	
        		<p id="a<?php echo $field->name; ?>" class="form-row <?php echo $field->width; ?> <?php echo $clss; ?>">
					<label for="<?php echo $field->name; ?>"><?php _e( $field->label, 'woocommerce' ); ?> 
						<?php if($field->is_required == 1) { ?> <span class="required">*</span> <?php } ?>
					</label>
					<input type="text" class="input-text color_sepctrum" name="<?php echo $field->name; ?>" id="<?php echo $field->name; ?>" value="<?php if ( ! empty( $_POST[$field->name] ) ) esc_attr_e( $_POST[$field->name] ); ?>" placeholder="<?php echo $field->placeholder; ?>" />
					<?php if(isset($field->message) && $field->message!='') { ?>
						<span style="width:100%;float: left"><?php echo $field->message; ?></span>
					<?php } ?>
				</p>

        	<?php } ?> 

        	<script type="text/javascript">
        		jQuery(document).ready(function($) { 
        			
					<?php if($field->showif == 'show') { 

						if($field->pcondition == 'not-empty') { ?>

							$("#registration_field_<?php echo $field->condition_field ?>").change(function(){
								$("#a<?php echo $field->name ?>").show();
							});
							$("#a<?php echo $field->name ?>").show();

							$('input:radio').change(function() {

								if ($(this).is(':checked')) {
									var aa = $(this).val();
						        }

								if(aa != '') {
							 		$("#a<?php echo $field->name ?>").show();
							 	} else {
							 		$("#a<?php echo $field->name ?>").hide();
							 	}
						 	});

						 	var aa = $("input[type='radio']:checked").val();
						 	if(aa == "<?php echo $field->condition_value; ?>") {
						 		$("#a<?php echo $field->name ?>").show();
						 	} else {
						 		$("#a<?php echo $field->name ?>").hide();
						 	}


						<?php } else if($field->pcondition == 'equal-to') { ?> 

							$("#registration_field_<?php echo $field->condition_field ?>").change(function(){ 
							 	if($("#registration_field_<?php echo $field->condition_field ?>").val() == "<?php echo $field->condition_value; ?>") {
							 		$("#a<?php echo $field->name ?>").show();
							 	} else {
							 		$("#a<?php echo $field->name ?>").hide();
							 	}
							});

							if($("#registration_field_<?php echo $field->condition_field ?>").val() == "<?php echo $field->condition_value; ?>") {
						 		$("#a<?php echo $field->name ?>").show();
						 	} else {
						 		$("#a<?php echo $field->name ?>").hide();
						 	}

							$('input:radio').change(function() {

								if ($(this).is(':checked')) {
									var aa = $(this).val();
						        }


								if(aa == "<?php echo $field->condition_value; ?>") {
							 		$("#a<?php echo $field->name ?>").show();
							 	} else {
							 		$("#a<?php echo $field->name ?>").hide();
							 	}
						 	});

						 	var aa = $("input[type='radio']:checked").val();
						 	if(aa == "<?php echo $field->condition_value; ?>") {
						 		$("#a<?php echo $field->name ?>").show();
						 	} else {
						 		$("#a<?php echo $field->name ?>").hide();
						 	}

							

						<?php } else if($field->pcondition == 'not-equal-to') { ?>

							$("#registration_field_<?php echo $field->condition_field ?>").change(function(){
							 	if($("#registration_field_<?php echo $field->condition_field ?>").val() != "<?php echo $field->condition_value; ?>") {
							 		$("#a<?php echo $field->name ?>").show();
							 	} else {
							 		$("#a<?php echo $field->name ?>").hide();
							 	}
							});

							if($("#registration_field_<?php echo $field->condition_field ?>").val() != "<?php echo $field->condition_value; ?>") {
						 		$("#a<?php echo $field->name ?>").show();
						 	} else {
						 		$("#a<?php echo $field->name ?>").hide();
						 	}

							$('input:radio').change(function() {

								if ($(this).is(':checked')) {
									var aa = $(this).val();
						        }


								if(aa != "<?php echo $field->condition_value; ?>") {
							 		$("#a<?php echo $field->name ?>").show();
							 	} else {
							 		$("#a<?php echo $field->name ?>").hide();
							 	}
						 	});


						 	var aa = $("input[type='radio']:checked").val();
						 	if(aa != "<?php echo $field->condition_value; ?>") {
						 		$("#a<?php echo $field->name ?>").show();
						 	} else {
						 		$("#a<?php echo $field->name ?>").hide();
						 	}


						<?php } else if($field->pcondition == 'checked') { ?>

							$("#registration_field_<?php echo $field->condition_field ?>").change(function(){ 

								if ($(this).is(':checked')) {
						           $("#a<?php echo $field->name ?>").show();
						        } else {
						        	$("#a<?php echo $field->name ?>").hide();
						        }
							});

							if ($("#registration_field_<?php echo $field->condition_field ?>").is(':checked')) { 
								$("#a<?php echo $field->name ?>").show();
							} else {
								$("#a<?php echo $field->name ?>").hide();
							}



						<?php } ?>

		        			
	        		<?php } elseif($field->showif == 'hide') { ?>

	        			<?php if($field->pcondition == 'not-empty') { ?>

							$("#registration_field_<?php echo $field->condition_field ?>").change(function(){
								$("#a<?php echo $field->name ?>").hide();
							});

							$("#a<?php echo $field->name ?>").hide();

							
							$('input:radio').change(function() {

								if ($(this).is(':checked')) {
									var aa = $(this).val();
						        }

								if(aa != '') {
							 		$("#a<?php echo $field->name ?>").hide();
							 	} else {
							 		$("#a<?php echo $field->name ?>").show();
							 	}
						 	});


						 	var aa = $("input[type='radio']:checked").val();
						 	if(aa == "<?php echo $field->condition_value; ?>") {
						 		$("#a<?php echo $field->name ?>").hide();
						 	} else {
						 		$("#a<?php echo $field->name ?>").show();
						 	}


						<?php } else if($field->pcondition == 'equal-to') { ?>
							$("#registration_field_<?php echo $field->condition_field ?>").change(function(){
							 	if($("#registration_field_<?php echo $field->condition_field ?>").val() == "<?php echo $field->condition_value; ?>") {
							 		$("#a<?php echo $field->name ?>").hide();
							 	} else {
							 		$("#a<?php echo $field->name ?>").show();
							 	}
							});

							if($("#registration_field_<?php echo $field->condition_field ?>").val() == "<?php echo $field->condition_value; ?>") {
						 		$("#a<?php echo $field->name ?>").hide();
						 	} else {
						 		$("#a<?php echo $field->name ?>").show();
						 	}

							$('input:radio').change(function() {

								if ($(this).is(':checked')) {
									var aa = $(this).val();
						        }


								if(aa == "<?php echo $field->condition_value; ?>") {
							 		$("#a<?php echo $field->name ?>").hide();
							 	} else {
							 		$("#a<?php echo $field->name ?>").show();
							 	}
						 	});

						 	var aa = $("input[type='radio']:checked").val();
						 	if(aa == "<?php echo $field->condition_value; ?>") {
						 		$("#a<?php echo $field->name ?>").hide();
						 	} else {
						 		$("#a<?php echo $field->name ?>").show();
						 	}
							

						<?php } else if($field->pcondition == 'not-equal-to') { ?>

							$("#registration_field_<?php echo $field->condition_field ?>").change(function(){
							 	if($("#registration_field_<?php echo $field->condition_field ?>").val() != "<?php echo $field->condition_value; ?>") {
							 		$("#a<?php echo $field->name ?>").hide();
							 	} else {
							 		$("#a<?php echo $field->name ?>").show();
							 	}
							});

							if($("#registration_field_<?php echo $field->condition_field ?>").val() != "<?php echo $field->condition_value; ?>") {
						 		$("#a<?php echo $field->name ?>").hide();
						 	} else {
						 		$("#a<?php echo $field->name ?>").show();
						 	}


							$('input:radio').change(function() {

								if ($(this).is(':checked')) {
									var aa = $(this).val();
						        }


								if(aa != "<?php echo $field->condition_value; ?>") {
							 		$("#a<?php echo $field->name ?>").hide();
							 	} else {
							 		$("#a<?php echo $field->name ?>").show();
							 	}
						 	});

						 	var aa = $("input[type='radio']:checked").val();
						 	if(aa != "<?php echo $field->condition_value; ?>") {
						 		$("#a<?php echo $field->name ?>").hide();
						 	} else {
						 		$("#a<?php echo $field->name ?>").show();
						 	}


							


						<?php } else if($field->pcondition == 'checked') { ?>

							$("#registration_field_<?php echo $field->condition_field ?>").change(function(){ 

								if ($(this).is(':checked')) {
						           $("#a<?php echo $field->name ?>").hide();
						        } else {
						        	$("#a<?php echo $field->name ?>").show();
						        }
							});


							if ($("#registration_field_<?php echo $field->condition_field ?>").is(':checked')) { 
								$("#a<?php echo $field->name ?>").hide();
							} else {
								$("#a<?php echo $field->name ?>").show();
							}



						<?php } ?>
	        			
	        		<?php } else { ?>
	        			
		        	<?php } ?>
					
				});
        	</script>

        	<script>
				jQuery(".color_sepctrum").spectrum({
				    color: "#f00",
				    preferredFormat: "hex",
				});
			</script>

    	<?php } 
    	echo '<span class="form-row"><br></span>';
	}

    //return all default fields
	function phlox_get_default_field($value){
		global $wpdb;
        $table_name = $wpdb->prefix.'phlox_reg_default_fields';     
        $result = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM ".$table_name." WHERE name = %s", $value));      
        return $result;
	}

    //google captcha verification
	function phlox_google_captcha_check($value){
		$secret_key = self::get_recaptcha_secret_key();
       
		$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret_key.'&response='.$value);
		
		$responseData = json_decode($verifyResponse);

		if($responseData->success) {
			return "success";
		} else {
			return "error";
		}
	}

    //validation of registration fields and update
	function phlox_wooc_validate_register_fields($username, $email, $validation_errors){

   		global $woocommerce;
   		
		if(isset($_POST['pageID']) && $_POST['pageID']!='') {
			$current_page = $_POST['pageID'];
		} else {
			$current_page = 0;
		}
   		$check_page = wc_get_page_id( 'checkout' );
   		

   		//Default Fields
    	//First Name
    	$fieldData = self::phlox_get_default_field('first_name');

    	if( isset($_POST['first_name'] ) && empty( $_POST['first_name']) && $fieldData->status == 'enabled' && $fieldData->is_required == 1) {
			$validation_errors->add($fieldData->name.'_error', __($fieldData->label.' is required!', 'woocommerce' ) );
		}

		//Last Name
    	$fieldData = self::phlox_get_default_field('last_name');

    	if( isset( $_POST['last_name'] ) && empty( $_POST['last_name'] ) && $fieldData->status == 'enabled' && $fieldData->is_required == 1) {
			$validation_errors->add($fieldData->name.'_error', __($fieldData->label.' is required!', 'woocommerce' ) );
		}

		//Company
    	$fieldData = self::phlox_get_default_field('billing_company');

    	if( isset( $_POST['billing_company'] ) && empty( $_POST['billing_company'] ) && $fieldData->status == 'enabled' && $fieldData->is_required == 1) {
			$validation_errors->add($fieldData->name.'_error', __($fieldData->label.' is required!', 'woocommerce' ) );
		}

		//Country
    	$fieldData = self::phlox_get_default_field('billing_country');

    	if( isset( $_POST['billing_country'] ) && empty( $_POST['billing_country'] ) && $fieldData->status == 'enabled' && $fieldData->is_required == 1) {
			$validation_errors->add($fieldData->name.'_error', __($fieldData->label.' is required!', 'woocommerce' ) );
		}

		//Address 1
    	$fieldData = self::phlox_get_default_field('billing_address_1');

    	if( isset( $_POST['billing_address_1'] ) && empty( $_POST['billing_address_1'] ) && $fieldData->status == 'enabled' && $fieldData->is_required == 1) {
			$validation_errors->add($fieldData->name.'_error', __($fieldData->label.' is required!', 'woocommerce' ) );
		}

		//Address 2
    	$fieldData = self::phlox_get_default_field('billing_address_2');

    	if( isset( $_POST['billing_address_2'] ) && empty( $_POST['billing_address_2'] ) && $fieldData->status == 'enabled' && $fieldData->is_required == 1) {
			$validation_errors->add($fieldData->name.'_error', __($fieldData->label.' is required!', 'woocommerce' ) );
		}

		//City
    	$fieldData = self::phlox_get_default_field('billing_city');

    	if( isset( $_POST['billing_city'] ) && empty( $_POST['billing_city'] ) && $fieldData->status == 'enabled' && $fieldData->is_required == 1) {
			$validation_errors->add($fieldData->name.'_error', __($fieldData->label.' is required!', 'woocommerce' ) );
		}

		//State
    	$fieldData = self::phlox_get_default_field('billing_state');

    	if( isset( $_POST['billing_state'] ) && empty( $_POST['billing_state'] ) && $fieldData->status == 'enabled' && $fieldData->is_required == 1) {
			$validation_errors->add($fieldData->name.'_error', __($fieldData->label.' is required!', 'woocommerce' ) );
		}


		//PostCode
    	$fieldData = self::phlox_get_default_field('billing_postcode');

    	if( isset( $_POST['billing_postcode'] ) && empty( $_POST['billing_postcode'] ) && $fieldData->status == 'enabled' && $fieldData->is_required == 1) {
			$validation_errors->add($fieldData->name.'_error', __($fieldData->label.' is required!', 'woocommerce' ) );
		}

		//Phone
    	$fieldData = self::phlox_get_default_field('billing_phone');

    	if( isset( $_POST['billing_phone'] ) && empty( $_POST['billing_phone'] ) && $fieldData->status == 'enabled' && $fieldData->is_required == 1) {
			$validation_errors->add($fieldData->name.'_error', __($fieldData->label.' is required!', 'woocommerce'));
		}

        //all custom feilds
   		$fields = self::phlox_get_fields();
   		
    	foreach ($fields as $field) { 

    		if ( isset( $_POST[$field->name] ) && empty( $_POST[$field->name] ) && ($field->is_required == 1 && $field->showif == '') ) {
					
				$validation_errors->add($field->name.'_error', __($field->label.' is required!', 'woocommerce'));

			}


			if ( isset( $_POST[$field->name] ) && empty( $_POST[$field->name] ) && ($field->is_required == 1 && $field->showif != '') ) {

				
				if($field->pcondition!='checked') {

					if($_POST[$field->confition_field] == $field->condition_value) {
        				
        				$validation_errors->add($field->name.'_error', __($field->label.' is required!', 'woocommerce'));

        			}
    			} 

    			if($field->pcondition == 'checked' && $_POST[$field->condition_field] == 1) {

    				$validation_errors->add($field->name.'_error', __($field->label.' is required!', 'woocommerce'));

    			}
			}

    	}

    	foreach ($fields as $field) { 

    		if($field->type == 'multiselect') {
    			
    			if(!array_key_exists($field->name, $_POST)) {
        			if($field->is_required == 1 && $field->is_hide == 0) {
        				$validation_errors->add($field->name.'_error', __($field->label.' is required!', 'woocommerce'));
        			}
        		}
    		}else if($field->type == 'captcha'  && $current_page != 0) {
    			
    			if(isset($_POST['g-recaptcha-response']) && $_POST['g-recaptcha-response'] != '') {
    				$ccheck = self::phlox_google_captcha_check($_POST['g-recaptcha-response']);
    				if($ccheck == 'error') {
    					$validation_errors->add($field->name.'_error', __($field->label.' is required!', 'woocommerce'));
    				}
    			} else {
    				$validation_errors->add($field->name.'_error', __($field->label.' is required!', 'woocommerce'));
    			}
    		}else if($field->type=='file') { 
        		if(!array_key_exists($field->name, $_POST)) {
        			
                    if(array_key_exists($field->name, $_FILES)) {
                        if(isset($_FILES[$field->name]['name']) && empty($_FILES[$field->name]['name']) && $field->is_required == 1 && $field->is_hide == 0) {
                            $validation_errors->add($field->name.'_error', __($field->label.' is required!', 'woocommerce'));
                        }
                    }
        		}
    		} else {
                
                if($field->is_required == 1 && $field->is_hide == 0 && empty($_POST[$field->name])) {
                    $validation_errors->add($field->name.'_error', __($field->label.' is required!', 'woocommerce'));
                }    			
    		}
    	}    	

    	return $validation_errors;
	}

	function phlox_wooc_save_register_fields($customer_id){
		
   		//Default Fields

   		//First Name
		if ( isset( $_POST['first_name'] ) && $_POST['first_name']!='' ) {
			update_user_meta( $customer_id, 'first_name', sanitize_text_field( $_POST['first_name'] ) );
			update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( $_POST['first_name'] ) );
		}

	      //Last Name
		if ( isset( $_POST['last_name'] ) && $_POST['last_name']!='' ) {
			update_user_meta( $customer_id, 'last_name', sanitize_text_field( $_POST['last_name'] ) );
			update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( $_POST['last_name'] ) );
		}

	      //Company
		if ( isset( $_POST['billing_company'] ) ) {
			update_user_meta($customer_id, 'billing_company', sanitize_text_field( $_POST['billing_company']));
		}

          //country
		if ( isset( $_POST['billing_country'] ) ) {
			// Phone input filed which is used in WooCommerce
			update_user_meta($customer_id, 'billing_country', sanitize_text_field( $_POST['billing_country'] ) );
		}


          //address 1
		if ( isset( $_POST['billing_address_1'] ) ) {
			// Phone input filed which is used in WooCommerce
			update_user_meta($customer_id, 'billing_address_1', sanitize_text_field( $_POST['billing_address_1']));
		}

          //address 2
		if ( isset( $_POST['billing_address_2'] ) ) {
			// Phone input filed which is used in WooCommerce
			update_user_meta( $customer_id, 'billing_address_2', sanitize_text_field( $_POST['billing_address_2'] ) );
		}

          //city
	    if ( isset( $_POST['billing_city'] ) ) {
            // Phone input filed which is used in WooCommerce
            update_user_meta( $customer_id, 'billing_city', sanitize_text_field( $_POST['billing_city'] ) );
        }

          //state
        if ( isset( $_POST['billing_state'] ) ) {
			// Phone input filed which is used in WooCommerce
        	update_user_meta( $customer_id, 'billing_state', sanitize_text_field( $_POST['billing_state'] ) );
        }

          //postcode
        if ( isset( $_POST['billing_postcode'] ) ) {
			// Phone input filed which is used in WooCommerce
        	update_user_meta( $customer_id, 'billing_postcode', sanitize_text_field( $_POST['billing_postcode'] ) );
        }

          //phone
        if ( isset( $_POST['billing_phone'] ) ) {
			// Phone input filed which is used in WooCommerce
        	update_user_meta( $customer_id, 'billing_phone', sanitize_text_field( $_POST['billing_phone'] ) );
        }

        //all custom fields
   		$fields = self::phlox_get_fields();
    	foreach ($fields as $field) { 

    		if ( isset( $_POST[$field->name] ) || isset( $_FILES[$field->name] ) ) {

        		if($field->type == 'file') {

        			if($_FILES[$field->name]['name']!='') { 

						$file = time('m').$_FILES[$field->name]['name'];
						$target_path = PLUGIN_PATH.'uploaded_img/';
						$target_path = $target_path . $file;
						$temp = move_uploaded_file($_FILES[$field->name]['tmp_name'], $target_path);
						update_user_meta($customer_id,$field->name,PLUGIN_URL.'uploaded_img/'.$file);
							
					} 
					

        		} else if($field->type == 'multiselect') { 
        			$prefix = '';
        			$multi = '';
        			foreach ($_POST[$field->name] as $value) {
        				$multi .= $prefix.$value;
						$prefix = ', ';
        			}
        			update_user_meta( $customer_id, $field->name, $multi );

        		} else {

					update_user_meta( $customer_id, $field->name,  $_POST[$field->name]);
				}

    		}
    	}
	}

    //user dashboard edit profile fields
	public function endpoint_content() { 
		
		if(get_query_var( 'edit-profile') == 'profile') { 

            //show edit page
			$user_id = get_current_user_id();
			self::eo_extra_registration_form_edit($user_id);

		} else { 
            //show edit option
            ?>
		
		<div class="col2-set addresses">
			<header class="title">
				<?php if(self::get_profile_title() == ''){
					echo "<h3></h3>";
				}else{
					self::get_profile_title();
				} ?>
				<a href="<?php echo esc_url( wc_get_endpoint_url( 'edit-profile', 'profile' ) ); ?>" class="edit"><?php _e( 'Edit', 'eorf' ); ?></a>					
			</header>
		</div>
		<table class="shop_table shop_table_responsive my_account_orders">
		<tbody>
		<?php 
            //show all custom fields data
			$user_id = get_current_user_id();
			$fields =  self::phlox_get_fields();
			$value = get_user_meta($user_id);
			foreach ($fields as $field) {
				
				if($value!='') { ?>
					<tr class="order" style="text-align:left">
                        <?php if($field->type != 'captcha'){ ?>
						<td style="width:30%;"><b><?php echo $field->label; ?></b></td>
                        <?php } ?>
						<td>
							<?php //show file data
							if($field->type == 'file') { 
								$aa = PLUGIN_URL.'uploaded_img/'.$value[$field->name][0];
								$ext = pathinfo($aa, PATHINFO_EXTENSION);
								if($ext == 'pdf' || $ext == 'PDF') { ?>
									<a href="<?php echo PLUGIN_URL; ?>uploaded_img/<?php echo $value[$field->name][0]; ?>" target="_blank">
										<img src="<?php echo PLUGIN_URL; ?>images/pdf.png" width="150" height="150" title="Click to View" />
									</a>
								<?php } else { ?>
									<img src="<?php echo $value[$field->name][0]; ?>" width="150" height="150" />
								<?php } ?>
								<!-- checkbox data -->	
							<?php } else if($field->type=='checkbox' && @$value[$field->name][0]==1) { 
									echo "Yes";
							} else if($field->type=='checkbox' && @$value[$field->name][0]==0) {
									echo "No";
                            //select data
							} else if($field->type=='select' || $field->type=='radioselect') { 
								echo @$value[$field->name][0];
                            //multiselect
							} else if($field->type=='multiselect') {
								echo @$value[$field->name][0];

                            //color data
							} else if($field->type =='color') { ?>
								<div style="width:50px;height:30px;background-color: <?php echo $value[$field->name][0]; ?>"></div>
							<?php } elseif($field->type == 'captcha'){}else {
								echo @$value[$field->name][0];
							} ?>
						</td>
					</tr>					
				<?php }
			} 
		?>

		</tbody>
		</table>
		
		
	<?php } }

    //update registration fields
	function eo_extra_registration_form_edit($user_id) {  

        //show profile registration
        if(self::get_profile_title() == ''){
			echo "<h3></h3>";
		}else{
			self::get_profile_title();
		} ?>
        <form method="post" enctype="multipart/form-data" id="edform">
        	<?php $this->phlox_show_error_messages(); 

            //get all custom fields
        	$fields = self::phlox_get_fields();
        	$clss = '';
        	foreach ($fields as $field) { 

	        	if($field->showif == 'show') {
	    			$clss = 'hide';
	    		} elseif($field->showif == 'hide') {
	    			$clss = 'show';
	    		} else {
	    			$clss = '';
	    		}
        		
        		$value = get_user_meta($user_id);
        		//text field dada
        		if($field->type == 'text' && $field->is_hide == 0) { ?>

		        	<p id="a<?php echo $field->name; ?>"  class="form-row <?php echo $field->width; ?> <?php echo $clss; ?>">
						<label for="<?php echo $field->name; ?>"><?php _e( $field->label, 'woocommerce' ); ?> 
							<?php if($field->is_required == 1) { ?> <span class="required">*</span> <?php } ?>
						</label>
						<input type="text" class="input-text" name="<?php echo $field->name; ?>" id="<?php echo $field->name; ?>" value="<?php echo @$value[$field->name][0]; ?>" placeholder="<?php echo $field->placeholder; ?>" />
						<?php if(isset($field->message) && $field->message!='') { ?>
							<span style="width:100%;float: left"><?php echo $field->message; ?></span>
						<?php } ?>
					</p>
	        		
                <!-- textarea data -->
	        	<?php } else if($field->type == 'textarea' && $field->is_hide == 0) { ?>

		        	<p id="a<?php echo $field->name; ?>"  class="form-row <?php echo $field->width; ?> <?php echo $clss; ?>">
						<label for="<?php echo $field->name; ?>"><?php _e( $field->label, 'woocommerce' ); ?> 
							<?php if($field->is_required == 1) { ?> <span class="required">*</span> <?php } ?>
						</label>
						<textarea name="<?php echo $field->name; ?>" id="<?php echo $field->name; ?>" class="input-text" cols="5" rows="2" placeholder="<?php echo $field->placeholder; ?>"><?php echo $value[$field->name][0]; ?></textarea>
						<?php if(isset($field->message) && $field->message!='') { ?>
							<span style="width:100%;float: left"><?php echo $field->message; ?></span>
						<?php } ?>
					</p>

                <!-- seelct data -->
	        	<?php } else if($field->type == 'select' && $field->is_hide == 0) { ?>

		        	<p id="a<?php echo $field->name; ?>"  class="form-row <?php echo $field->width; ?> <?php echo $clss; ?>">
						<label for="<?php echo $field->name; ?>"><?php _e( $field->label, 'woocommerce' ); ?> 
							<?php if($field->is_required == 1) { ?> <span class="required">*</span> <?php } ?>
						</label>
						<select name="<?php echo $field->name; ?>" id="<?php echo $field->name; ?>">
						    <option value="">Please select value</option>
						<?php $options = self::phlox_get_select_options($field->field_id);
						$options = json_decode( $options[0]->meta_data, true );
							foreach($options as $option=>$val) {
						?>
							<option value="<?php echo $val; ?>"  <?php if($val == $value[$field->name][0]) { echo "selected"; } ?>>
								<?php echo $option; ?>
							</option>

						<?php } ?>

						</select>
						<?php if(isset($field->message) && $field->message!='') { ?>
							<span style="width:100%;float: left"><?php echo $field->message; ?></span>
						<?php } ?>
					</p>

	        	<!-- multislect data -->
                <?php } else if($field->type == 'multiselect' && $field->is_hide == 0) { ?>

	        		<p id="a<?php echo $field->name; ?>"  class="form-row <?php echo $field->width; ?> <?php echo $clss; ?>">
						<label for="<?php echo $field->name; ?>"><?php _e( $field->label, 'woocommerce' ); ?> 
							<?php if($field->is_required == 1) { ?> <span class="required">*</span> <?php } ?>
						</label>
						
						<select style="height:150px;" multiple="true" name="<?php echo $field->name; ?>[]" id="<?php echo $field->name; ?>">
							<?php $options = self::phlox_get_select_options($field->field_id);
								$options = json_decode( $options[0]->meta_data, true );
								$selected_vals = explode(',', $value[$field->name][0]);
								$new_arr = array();
								foreach ($selected_vals as $key => $valu) {
									$new_arr[] = trim($valu);
								}
								foreach($options as $option=>$val) {							
							?>
								<option value="<?php echo $val; ?>" <?php if(in_array($val, $new_arr)) { echo "selected"; } ?>><?php echo $option; ?></option>
							<?php } ?>
						</select>

						<?php if(isset($field->message) && $field->message!='') { ?>
							<span style="width:100%;float: left;"><?php echo $field->message; ?></span>
						<?php } ?>
					</p>

	        	<!-- checkbox data -->
                <?php } else if($field->type == 'checkbox' && $field->is_hide == 0) { ?>

		        	<p id="a<?php echo $field->name; ?>"  class="form-row <?php echo $field->width; ?> <?php echo $clss; ?>">
						
						<input type="checkbox" id="c<?php echo $field->name; ?>" name="<?php echo $field->name; ?>" value="1" <?php if(@$value[$field->name][0] == 1){ echo "checked"; } ?> class="input-checkbox">

						<?php _e( $field->label, 'woocommerce' ); ?> 
							<?php if($field->is_required == 1) { ?> <span class="required">*</span> <?php } ?>

							<?php if(isset($field->message) && $field->message!='') { ?>
							<span style="width:100%;float: left"><?php echo $field->message; ?></span>
						<?php } ?>
						
					</p>

	        	<!-- radio button -->
                <?php } else if($field->type == 'radio' && $field->is_hide == 0) { ?>

		        	<p id="a<?php echo $field->name; ?>"  class="form-row <?php echo $field->width; ?> <?php echo $clss; ?>">
		        		<label for="<?php echo $field->name; ?>"><?php _e( $field->label, 'woocommerce' ); ?> 
							<?php if($field->is_required == 1) { ?> <span class="required">*</span> <?php } ?>
						</label>
						<?php $options = self::phlox_get_select_options($field->field_id);
						    $options = json_decode( $options[0]->meta_data, true );
						    $selected_vals = explode(',', $value[$field->name][0]);
							$new_arr = array();
							foreach ($selected_vals as $key => $valu) {
								$new_arr[] = trim($valu);
							}
					        //print_r($new_arr);exit;
							foreach($options as $option => $val) {
						?>

						<input type="radio" name="<?php echo $field->name; ?>" value="<?php echo $val; ?>" <?php if(in_array($val, $new_arr)) { echo "checked"; } ?> class="input-checkbox"> <?php echo $option; ?>

						<?php } ?>
						<?php if(isset($field->message) && $field->message!='') { ?>
							<span style="width:100%;float: left"><?php echo $field->message; ?></span>
						<?php } ?>
		        	</p>

	        	<!-- datepicker -->
                <?php } else if($field->type == 'datepicker' && $field->is_hide == 0) { ?>

		        	<p id="a<?php echo $field->name; ?>"  class="form-row <?php echo $field->width; ?> <?php echo $clss; ?>">
						<label for="<?php echo $field->name; ?>"><?php _e( $field->label, 'woocommerce' ); ?> 
							<?php if($field->is_required == 1) { ?> <span class="required">*</span> <?php } ?>
						</label>
						<input type="text" class="input-text datepick" name="<?php echo $field->name; ?>" id="<?php echo $field->name; ?>" value="<?php echo $value[$field->name][0]; ?>" placeholder="<?php echo $field->placeholder; ?>" />
						<?php if(isset($field->message) && $field->message!='') { ?>
							<span style="width:100%;float: left"><?php echo $field->message; ?></span>
						<?php } ?>
					</p>

	        	<!-- timepicker -->
                <?php } else if($field->type == 'timepicker' && $field->is_hide == 0) { ?>

		        	<p id="a<?php echo $field->name; ?>"  class="form-row <?php echo $field->width; ?> <?php echo $clss; ?>">
						<label for="<?php echo $field->name; ?>"><?php _e( $field->label, 'woocommerce' ); ?> 
							<?php if($field->is_required == 1) { ?> <span class="required">*</span> <?php } ?>
						</label>
						<input type="text" class="input-text timepick" name="<?php echo $field->name; ?>" id="<?php echo $field->name; ?>" value="<?php echo $value[$field->name][0]; ?>" placeholder="<?php echo $field->placeholder; ?>" />
						<?php if(isset($field->message) && $field->message!='') { ?>
							<span style="width:100%;float: left"><?php echo $field->message; ?></span>
						<?php } ?>
					</p>

	        	<!-- password -->
                <?php } else if($field->type == 'password' && $field->is_hide == 0) { ?>

		        	<p id="a<?php echo $field->name; ?>"  class="form-row <?php echo $field->width; ?> <?php echo $clss; ?>">
						<label for="<?php echo $field->name; ?>"><?php _e( $field->label, 'woocommerce' ); ?> 
							<?php if($field->is_required == 1) { ?> <span class="required">*</span> <?php } ?>
						</label>
						<input type="password" class="input-text" name="<?php echo $field->name; ?>" id="<?php echo $field->name; ?>" value="<?php echo $value[$field->name][0]; ?>" placeholder="<?php echo $field->placeholder; ?>" />
						<?php if(isset($field->message) && $field->message!='') { ?>
							<span style="width:100%;float: left"><?php echo $field->message; ?></span>
						<?php } ?>
					</p>

	        	<!-- file -->
                <?php } else if($field->type == 'file' && $field->is_hide == 0) { ?>

		        	<p id="a<?php echo $field->name; ?>"  class="form-row <?php echo $field->width; ?> <?php echo $clss; ?>">
						<label for="<?php echo $field->name; ?>"><?php _e('Current', 'eorf') ?> <?php echo $field->label; ?></label>
						
						<?php 
						
							$aa = $value[$field->name][0];
							$ext = pathinfo($aa, PATHINFO_EXTENSION);
							if($ext == 'pdf' || $ext == 'PDF') { ?>
								<a href="<?php echo $value[$field->name][0]; ?>" target="_blank">
									<img src="<?php echo PLUGIN_URL; ?>images/pdf.png" width="150" height="150" title="Click to View" />
								</a>
							<?php } else { ?>
								<img src="<?php echo $value[$field->name][0]; ?>" width="150" height="150" />
							<?php } ?>

						<input type="hidden" class="input-text" value="<?php echo $value[$field->name][0]; ?>" id="curr_<?php echo $field->name; ?>" name="curr_<?php echo $field->name; ?>">
						
					</p>

		        	<p id="a<?php echo $field->name; ?>" class="form-row <?php echo $field->width; ?> <?php echo $clss; ?>">
						<label for="<?php echo $field->name; ?>"><?php _e( $field->label, 'woocommerce' ); ?> 
							
						</label>
						
						<input type="file" class="input-text" name="<?php echo $field->name; ?>" id="<?php echo $field->name; ?>" />
						<?php if(isset($field->message) && $field->message!='') { ?>
							<span style="width:100%;float: left"><?php echo $field->message; ?></span>
						<?php } ?>
						
						<span style="width:100%; display:none; color:red; float: left" id = "typem<?php echo $field->name; ?>">
							<?php _e('File type not allowed!', 'eorf') ?>
						</span>
						<span style="width:100%; display:none; color:red; float: left" id = "sizem<?php echo $field->name; ?>">
							<?php _e('File size exceeded the maximum size!', 'eorf') ?>
						</span>
						<img style = "display:none;" id="im<?php echo $field->name; ?>" src="#" alt="" width="150" />

						<script type="text/javascript">

							function readURL(input,id) { 
								jQuery('#typem'+id).hide();
								jQuery('#sizem'+id).hide();
								jQuery('#im'+id).attr('src', '');
						        if (input.files && input.files[0]) { 

						        	if(!(input.files[0].type == 'application/pdf' || input.files[0].type == 'image/jpeg' || input.files[0].type == 'image/gif' || input.files[0].type == 'image/png')) {
						        		jQuery('#typem'+id).show();
						        		jQuery("input[name=save_profile]").prop('disabled', true);
						        	} else if(input.files[0].size > 5000000) {

						        		jQuery('#sizem'+id).show();
						        		jQuery("input[name=save_profile]").prop('disabled', true);

						        	} else {

						        		jQuery("input[name=save_profile]").prop('disabled', false);
						        		jQuery('#im'+id).show();
							            var reader = new FileReader();
							            
							            reader.onload = function (e) {
							                jQuery('#im'+id).attr('src', e.target.result);
							            }
							            
							            reader.readAsDataURL(input.files[0]);
						       	 	}
						        }
						    }
						    
						    jQuery("#<?php echo $field->name; ?>").change(function(){
						       //readURL(this,'<?php echo $field->name; ?>');
						        //jQuery('#'+<?php echo $field->name ?>);.val(this);
						    });

						</script>

					</p>
				
	        	<!-- numberic -->
                <?php } else if($field->type == 'numeric' && $field->is_hide == 0) { ?>
	        	
	        		<p id="a<?php echo $field->name; ?>"  class="form-row <?php echo $field->width; ?> <?php echo $clss; ?>">
						<label for="<?php echo $field->name; ?>"><?php _e( $field->label, 'woocommerce' ); ?> 
							<?php if($field->is_required == 1) { ?> <span class="required">*</span> <?php } ?>
						</label>
						<input type="number" class="input-text" name="<?php echo $field->name; ?>" id="<?php echo $field->name; ?>" value="<?php echo $value[$field->name][0]; ?>" placeholder="<?php echo $field->placeholder; ?>" />
						<?php if(isset($field->message) && $field->message!='') { ?>
							<span style="width:100%;float: left"><?php echo $field->message; ?></span>
						<?php } ?>
					</p>

	        	<!-- color -->
                <?php } else if($field->type == 'color' && $field->is_hide == 0) { ?>
	        	
	        		<p id="a<?php echo $field->name; ?>"  class="form-row <?php echo $field->width; ?> <?php echo $clss; ?>">
						<label for="<?php echo $field->name; ?>"><?php _e( $field->label, 'woocommerce' ); ?> 
							<?php if($field->is_required == 1) { ?> <span class="required">*</span> <?php } ?>
						</label>
						<input type="text" class="input-text color_spectrum" name="<?php echo $field->name; ?>" id="<?php echo $field->name; ?>" value="<?php echo @$value[$field->name][0]; ?>" placeholder="<?php echo $field->placeholder; ?>" />
						<?php if(isset($field->message) && $field->message!='') { ?>
							<span style="width:100%;float: left"><?php echo $field->message; ?></span>
						<?php } ?>
					</p>

					<script>
						
					jQuery(".color_spectrum").spectrum({
					    color: "<?php echo $value[$field->name][0]; ?>",
					    preferredFormat: "hex",
					});

					</script>
	        	<?php } ?>

	        	<script type="text/javascript">
	        		jQuery(document).ready(function($) { 
	        			
						<?php if($field->showif == 'show') { ?>

							<?php if($field->pcondition == 'not-empty') { ?>
								$("#registration_field_<?php echo $field->condition_field ?>").change(function(){
									$("#a<?php echo $field->name ?>").show();
								});

								$("#a<?php echo $field->name ?>").show();

								$('input:radio').change(function() {

									if ($(this).is(':checked')) {
										var aa = $(this).val();
							        }

									if(aa != '') {
								 		$("#a<?php echo $field->name ?>").show();
								 	} else {
								 		$("#a<?php echo $field->name ?>").hide();
								 	}
							 	});

							 	var aa = $("input[type='radio']:checked").val();
							 	if(aa == "<?php echo $field->condition_value; ?>") {
							 		$("#a<?php echo $field->name ?>").show();
							 	} else {
							 		$("#a<?php echo $field->name ?>").hide();
							 	}


							<?php } else if($field->pcondition == 'equal-to') { ?>
								$("#registration_field_<?php echo $field->condition_field ?>").change(function(){
								 	if($("#registration_field_<?php echo $field->condition_field ?>").val() == "<?php echo $field->condition_value; ?>") {
								 		$("#a<?php echo $field->name ?>").show();
								 	} else {
								 		$("#a<?php echo $field->name ?>").hide();
								 	}
								});



								if($("#registration_field_<?php echo $field->condition_field ?>").val() == "<?php echo $field->condition_value; ?>") {
									
							 		$("#a<?php echo $field->name ?>").show();
							 	} else {
							 		$("#a<?php echo $field->name ?>").hide();
							 	}

								$('input:radio').change(function() {

									if ($(this).is(':checked')) {
										var aa = $(this).val();
							        }


									if(aa == "<?php echo $field->condition_value; ?>") {
								 		$("#a<?php echo $field->name ?>").show();
								 	} else {
								 		$("#a<?php echo $field->name ?>").hide();
								 	}
							 	});

							 	var aa = $("input[type='radio']:checked").val();
							 	if(aa == "<?php echo $field->condition_value; ?>") {
							 		$("#a<?php echo $field->name ?>").show();
							 	} else {
							 		$("#a<?php echo $field->name ?>").hide();
							 	}

								

							<?php } else if($field->pcondition == 'not-equal-to') { ?>

								$("#registration_field_<?php echo $field->condition_field ?>").change(function(){
								 	if($("#registration_field_<?php echo $field->condition_field ?>").val() != "<?php echo $field->condition_value; ?>") {
								 		$("#a<?php echo $field->name ?>").show();
								 	} else {
								 		$("#a<?php echo $field->name ?>").hide();
								 	}
								});

								if($("#<?php echo $field->condition_field ?>").val() != "<?php echo $field->condition_value; ?>") {
							 		$("#a<?php echo $field->name ?>").show();
							 	} else {
							 		$("#a<?php echo $field->name ?>").hide();
							 	}

								$('input:radio').change(function() {

									if ($(this).is(':checked')) {
										var aa = $(this).val();
							        }


									if(aa != "<?php echo $field->condition_value; ?>") {
								 		$("#a<?php echo $field->name ?>").show();
								 	} else {
								 		$("#a<?php echo $field->name ?>").hide();
								 	}
							 	});


							 	var aa = $("input[type='radio']:checked").val();
							 	if(aa != "<?php echo $field->condition_value; ?>") {
							 		$("#a<?php echo $field->name ?>").show();
							 	} else {
							 		$("#a<?php echo $field->name ?>").hide();
							 	}


							<?php } else if($field->pcondition == 'checked') { ?>

								$("#registration_field_<?php echo $field->condition_field ?>").change(function(){ 

									if ($(this).is(':checked')) {
							           $("#a<?php echo $field->name ?>").show();
							        } else {
							        	$("#a<?php echo $field->name ?>").hide();
							        }
								});

								if ($("registration_field_<?php echo $field->condition_field ?>").is(':checked')) { 
									$("#a<?php echo $field->name ?>").show();
								} else {
									$("#a<?php echo $field->name ?>").hide();
								}



							<?php } ?>

			        			
		        		<?php } elseif($field->showif == 'hide') { ?>

		        			<?php if($field->pcondition == 'not-empty') { ?>
								$("#registration_field_<?php echo $field->condition_field ?>").change(function(){
									$("#a<?php echo $field->name ?>").hide();
								});

								$("#a<?php echo $field->name ?>").hide();

								
								$('input:radio').change(function() {

									if ($(this).is(':checked')) {
										var aa = $(this).val();
							        }

									if(aa != '') {
								 		$("#a<?php echo $field->name ?>").hide();
								 	} else {
								 		$("#a<?php echo $field->name ?>").show();
								 	}
							 	});


							 	var aa = $("input[type='radio']:checked").val();
							 	if(aa == "<?php echo $field->condition_value; ?>") {
							 		$("#a<?php echo $field->name ?>").hide();
							 	} else {
							 		$("#a<?php echo $field->name ?>").show();
							 	}


							<?php } else if($field->pcondition == 'equal-to') { ?>
								$("#registration_field_<?php echo $field->condition_field ?>").change(function(){
								 	if($("#registration_field_<?php echo $field->condition_field ?>").val() == "<?php echo $field->condition_value; ?>") {
								 		$("#a<?php echo $field->name ?>").hide();
								 	} else {
								 		$("#a<?php echo $field->name ?>").show();
								 	}
								});

								if($("#registration_field_<?php echo $field->condition_field ?>").val() == "<?php echo $field->condition_value; ?>") {
							 		$("#a<?php echo $field->name ?>").hide();
							 	} else {
							 		$("#a<?php echo $field->name ?>").show();
							 	}

								$('input:radio').change(function() {

									if ($(this).is(':checked')) {
										var aa = $(this).val();
							        }


									if(aa == "<?php echo $field->condition_value; ?>") {
								 		$("#a<?php echo $field->name ?>").hide();
								 	} else {
								 		$("#a<?php echo $field->name ?>").show();
								 	}
							 	});

							 	var aa = $("input[type='radio']:checked").val();
							 	if(aa == "<?php echo $field->condition_value; ?>") {
							 		$("#a<?php echo $field->name ?>").hide();
							 	} else {
							 		$("#a<?php echo $field->name ?>").show();
							 	}


								

							<?php } else if($field->pcondition == 'not-equal-to') { ?>

								$("#registration_field_<?php echo $field->condition_field ?>").change(function(){
								 	if($("#registration_field_<?php echo $field->condition_field ?>").val() != "<?php echo $field->condition_value; ?>") {
								 		$("#a<?php echo $field->name ?>").hide();
								 	} else {
								 		$("#a<?php echo $field->name ?>").show();
								 	}
								});

								if($("#registration_field_<?php echo $field->condition_field ?>").val() != "<?php echo $field->condition_value; ?>") {
							 		$("#a<?php echo $field->name ?>").hide();
							 	} else {
							 		$("#a<?php echo $field->name ?>").show();
							 	}


								$('input:radio').change(function() {

									if ($(this).is(':checked')) {
										var aa = $(this).val();
							        }


									if(aa != "<?php echo $field->condition_value; ?>") {
								 		$("#a<?php echo $field->name ?>").hide();
								 	} else {
								 		$("#a<?php echo $field->name ?>").show();
								 	}
							 	});

							 	var aa = $("input[type='radio']:checked").val();
							 	if(aa != "<?php echo $field->condition_value; ?>") {
							 		$("#a<?php echo $field->name ?>").hide();
							 	} else {
							 		$("#a<?php echo $field->name ?>").show();
							 	}


								
							<?php } else if($field->pcondition == 'checked') { ?>

								$("#registration_field_<?php echo $field->condition_field ?>").change(function(){ 

									if ($(this).is(':checked')) {
							           $("#a<?php echo $field->name ?>").hide();
							        } else {
							        	$("#a<?php echo $field->name ?>").show();
							        }
								});


								if ($("#registration_field_<?php echo $field->condition_field ?>").is(':checked')) { 
									$("#a<?php echo $field->name ?>").hide();
								} else {
									$("#a<?php echo $field->name ?>").show();
								}



							<?php } ?>
		        			
		        		<?php } else { ?>
		        			
		        		<?php } ?>
						
					});
	        	</script>



	        	<?php if($field->is_readonly == 1) { ?>
	        		<script type="text/javascript">document.getElementById("<?php echo $field->name ?>").disabled = true; </script>
	        	<?php } ?>

        	<?php } ?>

        	<!--<input type="hidden" name="action" value="SubmitRegForm" />-->
			<input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />

			<p>
				<input type="button" value="<?php _e('Save Changes', 'phlox') ?>" name="save_profile" class="button" onclick="updateProfile();">				

			</p>

        </form>


        <script type="text/javascript">
        	/*update all custom fields data*/
        	function updateProfile() {

        		var data2 = jQuery('#edform').serialize();
				var ajaxurl = '<?php echo admin_url( 'admin-ajax.php'); ?>';

				jQuery.ajax({
				    type: 'POST',
				    url: ajaxurl,
				    data: data2 + '&action=phlox_update_all_data',
				    success: function() {
				        window.location.reload(true);
				    }
				});
	
        	}

        </script>
        	
    <?php }

    //show errors when validating
    function phlox_show_error_messages() {
		if($codes = self::phlox_errors()->get_error_codes()) {
			echo '<ul class="woocommerce-error">';
			    // Loop error codes and display errors
			   foreach($codes as $code){
			        $message = self::phlox_errors()->get_error_message($code);
			         $aa = explode('*', $message);
			        echo '<li>' ._e($aa[0].' ', 'eorf').' '._e($aa[1], 'eorf').'</li>';
			    }
			echo '</ul>';
		}	
	}

    //show errors when validating
	function phlox_errors() {
	    static $wp_error; // Will hold global variable safely
	    return isset($wp_error) ? $wp_error : ($wp_error = new WP_Error(null, null, null));
	}

    //update alla custom fields
	function phlox_update_all_data() {

   		$user_id = get_current_user_id();		
		$fields = self::phlox_get_fields();

    	foreach ($fields as $field) { 

    		if($field->type == 'captcha') { 
			} else {
    		if($field->is_readonly!=1) {
                
    			if ( isset( $_POST[$field->name] ) && empty( $_POST[$field->name] ) && ($field->is_required == 1 && $field->showif != '') && $field->type != 'file' ) {

				
					if($field->pcondition!='checked') {

						if($_POST[$field->condition_field] == $field->condition_value) {  				
	        				//show errors
	        				self::phlox_errors()->add( $field->name.'_error', __($field->label." is required!", "woocommerce"  ));
	        			}
        			} 

        			if((isset($_POST[$field->conditio_field]) && $_POST[$field->condition_field] == 1) && ($field->pcondition == 'checked')) {
                        //show errors
        				self::phlox_errors()->add( $field->name.'_error', __($field->label." is required!", "woocommerce"  ));

        			}
				}
    
        		if ( isset( $_POST[$field->name] ) && empty( $_POST[$field->name] ) && ($field->is_required == 1) && $field->type != 'file'  && $field->showif == '' ) {
                    //show errors
					self::phlox_errors()->add($field->name.'_error', __($field->label.' is required!', 'woocommerce'));
				} else { 
				    
			         //show file 
	        		if($field->type == 'file') {
                        //print_r($_FILES);exit;
						if(@$_FILES[$field->name]['name']!='') {								
                            
							$file = time('m').$_FILES[$field->name]['name'];
							$target_path = PLUGIN_PATH.'uploaded_img/';
							$target_path = $target_path . $file;
							$temp = move_uploaded_file($_FILES[$field->name]['tmp_name'], $target_path);
							//echo $file;exit;
							update_user_meta($user_id,$field->name, PLUGIN_URL.'uploaded_img/'.$file);
							
						} else {
							$file = $_POST['curr_'.$field->name];
							update_user_meta($user_id,$field->name, $file);
						}
                        
						
	        		}
                    
					if ( isset( $_POST[$field->name] ) &&  $field->type != 'file') {
                        

                        //show multiselect
		        		if($field->type == 'multiselect') { 
		        			$prefix = '';
		        			$multi = '';
		        			foreach ($_POST[$field->name] as $value) {
		        				$multi .= $prefix.$value;
	    						$prefix = ', ';
		        			}
		        			update_user_meta( $user_id, $field->name, $multi );

		        		} else { 
		        			
							update_user_meta( $user_id, $field->name,  $_POST[$field->name] );
						}

	        		} else {
	        		    if($field->type != 'file'){
	        		        update_user_meta( $user_id, $field->name, '' );    
	        		    }
	        			
	        		}


				}

        	} }
    	}
	}

    //show default fields
	public function phlox_show_billing_fields($checkout) { 

		if(!is_user_logged_in()) { 

			$fields = $checkout->get_checkout_fields();
            //get fields
        	$bfields = self::phlox_get_fields();
        	$clss = '';

        	foreach($bfields as $bfield) {        				

				if($bfield->showif == 'show') {
        			$clss = 'hide';
        		} elseif($bfield->showif == 'hide') {
        			$clss = 'show';
        		} else {
        			$clss = '';
        		}

                //get option for select fields
        		if($bfield->type == 'select') {
			   		$opts = self::phlox_get_select_options($bfield->field_id);
					$opts = json_decode( $opts[0]->meta_data, true );
					$options[] = "Please select a value";
			   		foreach ($opts as $opt => $val) {			   			
			   			$options[$opt] = $val;
			   		}
		   		}

                //get options for radio fields
		   		if($bfield->type == 'radio') {
			   		$opts = self::phlox_get_select_options($bfield->field_id);
					$opts = json_decode( $opts[0]->meta_data, true );
			   		foreach ($opts as $opt => $val) {
			   			
			   			$options1[$opt] = $val;
			   		}
		   		}

                //get options for multiselect fields
		   		if($bfield->type == 'multiselect') {
			   		$opts = self::phlox_get_select_options($bfield->field_id);
					$opts = json_decode( $opts[0]->meta_data, true );
			   		foreach ($opts as $opt => $val) {
			   			
			   			$options2[$opt] = $val;
			   		}
		   		}

                //show all text feilds
        		if($bfield->type == 'text' && $bfield->is_hide == 0) {

        			if($bfield->is_required == 1) {

        				$fields['account'][$bfield->name] = array(
					        'label'         => __($bfield->label),
					        'required'      => true,
					        'placeholder'   => _x($bfield->placeholder, 'placeholder'),
					        'class'         => array(($bfield->width == 'full' ? 'full' : 'half'), $clss),
					        'clear'         => false,
					        'id'         	=> $bfield->name,
					        'type'			=> $bfield->type,
					        

					    );

        			} else {
	        			$fields['account'][$bfield->name] = array(
					        'label'         => __($bfield->label),
					        'placeholder'   => _x($bfield->placeholder, 'placeholder'),
					        'class'         => array(($bfield->width == 'full' ? 'full' : 'half'), $clss),
					        'clear'         => false,
					        'id'         	=> $bfield->name,
					        'type'			=> $bfield->type,
					        

					    );
        			}

                //show all textarea fields
        		} else if($bfield->type == 'textarea' && $bfield->is_hide == 0) {

        		 	if($bfield->is_required == 1) {

        		 		$fields['account'][$bfield->name] = array(
					        'label'         => __($bfield->label),
					        'required'      => true,
					        'placeholder'   => _x($bfield->placeholder, 'placeholder', 'eorf'),
					        'class'         => array(($bfield->width == 'full' ? 'full' : 'half'), $clss),
					        'clear'         => false,
					        'id'         	=> $bfield->name,
					        'type'			=> $bfield->type,
					        

					    );

        		 	} else {

        		 		$fields['account'][$bfield->name] = array(
					        'label'         => __($bfield->label, 'eorf'),
					        'placeholder'   => _x($bfield->placeholder, 'placeholder', 'eorf'),
					        'class'         => array(($bfield->width == 'full' ? 'full' : 'half'), $clss),
					        'clear'         => false,
					        'id'         	=> $bfield->name,
					        'type'			=> $bfield->type					        

					    );

        		 	}

                //show all select fields
        		} else if($bfield->type == 'select' && $bfield->is_hide == 0) {

        		 	if($bfield->is_required == 1) {

        		 		$fields['account'][$bfield->name] = array(
					        'label'         => __($bfield->label),
					        'required'      => true,
					        'placeholder'   => _x($bfield->placeholder, 'placeholder', 'eorf'),
					        'class'         => array(($bfield->width == 'full' ? 'full' : 'half'), $clss),
					        'clear'         => false,
					        'id'         	=> $bfield->name,
					        'type'			=> $bfield->type,
					        'options'     	=> $options,
				        );

        		 	} else {

        		 		$fields['account'][$bfield->name] = array(
					        'label'         => __($bfield->label, 'eorf'),
					        'placeholder'   => _x($bfield->placeholder, 'placeholder', 'eorf'),
					        'class'         => array(($bfield->width == 'full' ? 'full' : 'half'), $clss),
					        'clear'         => false,
					        'id'         	=> $bfield->name,
					        'type'			=> $bfield->type,
					        'options'     	=> $options,
				        );
        		 	}

		   		//show all radio fields
                } else if($bfield->type == 'radio' && $bfield->is_hide == 0) {

		   		 	if($bfield->is_required == 1) {

		   		 		$fields['account'][$bfield->name] = array(
					        'label'         => __($bfield->label),
					        'required'      => true,
					        'placeholder'   => _x($bfield->placeholder, 'placeholder', 'eorf'),
					        'class'         => array(($bfield->width == 'full' ? 'full' : 'half'), $clss),
					        'clear'         => false,
					        'type'			=> 'radio',
					        'options'     	=> $options1,

					    );

		   		 	} else {

		   		 		$fields['account'][$bfield->name] = array(
					        'label'         => __($bfield->label, 'eorf'),
					        'placeholder'   => _x($bfield->placeholder, 'placeholder', 'eorf'),
					        'class'         => array(($bfield->width == 'full' ? 'full' : 'half'), $clss),
					        'clear'         => false,
					        'type'			=> 'radio',
					        'options'     	=> $options1,

					    );
		   		 	}					   			

		   		//show all multiselect fields
                } else if($bfield->type == 'multiselect' && $bfield->is_hide == 0) {

		   		 	if($bfield->is_required == 1) {

		   		 		$fields['account'][$bfield->name.'[]'] = array(
					        'label'         => __($bfield->label),
					        'required'      => true,
					        'placeholder'   => _x($bfield->placeholder, 'placeholder', 'eorf'),
					        'class'         => array(($bfield->width == 'full' ? 'full' : 'half'), $clss),
					        'clear'         => false,
					        'id'         	=> $bfield->name,
					        'type'			=> $bfield->type,
					        'options'     	=> $options2,
				        );

		   		 	} else {

		   		 		$fields['account'][$bfield->name.'[]'] = array(
					        'label'         => __($bfield->label, 'eorf'),
					        'placeholder'   => _x($bfield->placeholder, 'placeholder', 'eorf'),
					        'class'         => array(($bfield->width == 'full' ? 'full' : 'half'), $clss),
					        'clear'         => false,
					        'id'         	=> $bfield->name,
					        'type'			=> $bfield->type,
					        'options'     	=> $options2,
				        );
		   		 	}
		   			
                //show all datepicker fields
		   		} else if($bfield->type == 'datepicker' && $bfield->is_hide == 0) {

		   			if($bfield->is_required == 1) {

		   				$fields['account'][$bfield->name] = array(

						   'type' => 'text',
						   'label'         => __($bfield->label),
						   'required'      => true,
						   'placeholder'   => _x($bfield->placeholder, 'placeholder', 'eorf'),
						   'input_class'   => array('input-text datepick'),
						   'class'         => array(($bfield->width == 'full' ? 'full' : 'half'), $clss),
						   'id'         	=> $bfield->name,
						   'clear'     => false
					    );

		   			} else {

		   				$fields['account'][$bfield->name] = array(

						   'type' => 'text',
						   'label'      => __($bfield->label, 'eorf'),
						   'placeholder'   => _x($bfield->placeholder, 'placeholder', 'eorf'),
						   'input_class'   => array('input-text datepick'),
						   'class'         => array(($bfield->width == 'full' ? 'full' : 'half'), $clss),
						   'id'         	=> $bfield->name,
						   'clear'     => false
					    );
		   			}

		   			
		   		//show all timepicker fields
                } else if($bfield->type == 'timepicker' && $bfield->is_hide == 0) {

		   			if($bfield->is_required == 1) {

		   				$fields['account'][$bfield->name] = array(

						   'type' => 'text',
						   'label'         => __($bfield->label),
						   'required'      => true,
						   'placeholder'   => _x($bfield->placeholder, 'placeholder', 'eorf'),
						   'input_class'   => array('input-text timepick'),
						   'class'         => array(($bfield->width == 'full' ? 'full' : 'half'), $clss),
						   'id'         	=> $bfield->name,
						   'clear'     => false
					    );

		   			} else {

		   				$fields['account'][$bfield->name] = array(

						   'type' => 'text',
						   'label'      => __($bfield->label, 'eorf'),
						   'placeholder'   => _x($bfield->placeholder, 'placeholder', 'eorf'),
						   'input_class'   => array('input-text timepick'),
						   'class'         => array(($bfield->width == 'full' ? 'full' : 'half'), $clss),
						   'id'         	=> $bfield->name,
						   'clear'     => false
					    );
		   			}
		   			
                //show all file fields
		   		} else if($bfield->type == 'file' && $bfield->is_hide == 0) {

		   		//show all checkbox fields
		   		} else if($bfield->type == 'checkbox' && $bfield->is_hide == 0) {

		   			if($bfield->is_required == 1) {

		   				$fields['account'][$bfield->name] = array(
					        'label'         => __($bfield->label),
					        'required'      => true,
					        'placeholder'   => _x($bfield->placeholder, 'placeholder', 'eorf'),
					        'class'         => array(($bfield->width == 'full' ? 'full' : 'half'), $clss),
					        'clear'         => false,
					        'id'         	=> 'c'.$bfield->name,
					        'type'			=> $bfield->type,
					        

					    );

					} else {

						$fields['account'][$bfield->name] = array(
					        'label'         => __($bfield->label, 'eorf'),
					        'placeholder'   => _x($bfield->placeholder, 'placeholder', 'eorf'),
					        'class'         => array(($bfield->width == 'full' ? 'full' : 'half'), $clss),
					        'clear'         => false,
					        'id'         	=> 'c'.$bfield->name,
					        'type'			=> $bfield->type,
					        

					    );
						
					}

			    //show all checkbox fields
				} else if($bfield->type == 'checkbox') { 

					if($bfield->is_required == 1) {

			   				$fields['account'][$bfield->name] = array(
						        'label'         => __($bfield->user_checkbox_label),
						        'required'      => true,
						        'placeholder'   => _x($bfield->placeholder, 'placeholder', 'eorf'),
						        'class'         => array(($bfield->width == 'full' ? 'full' : 'half'), $clss),
						        'clear'         => false,
						        'id'         	=> 'c'.$bfield->name,
						        'type'			=> 'checkbox'
						        

						    );

						} else {

							$fields['account'][$bfield->name] = array(
						        'label'         => __($bfield->user_checkbox_label, 'eorf'),
						        'placeholder'   => _x($bfield->placeholder, 'placeholder', 'eorf'),
						        'class'         => array(($bfield->width == 'full' ? 'full' : 'half'), $clss),
						        'clear'         => false,
						        'id'         	=> 'c'.$bfield->name,
						        'type'			=> 'checkbox'
						        

						    );
							
						}

                //show all numeric fields
				} else if($bfield->type == 'numeric' && $bfield->is_hide == 0) {

					if($bfield->is_required == 1) {

						$fields['account'][$bfield->name] = array(
					        'label'         => __($bfield->label),
					        'required'      => true,
					        'placeholder'   => _x($bfield->placeholder, 'placeholder', 'eorf'),
					        'class'         => array(($bfield->width == 'full' ? 'full' : 'half'), $clss),
					        'clear'         => false,
					        'id'         	=> $bfield->name,
					        'type'			=> 'number',
					        

					    );

					} else {

						$fields['account'][$bfield->name] = array(
					        'label'         => __($bfield->label, 'eorf'),
					        'placeholder'   => _x($bfield->placeholder, 'placeholder', 'eorf'),
					        'class'         => array(($bfield->width == 'full' ? 'full' : 'half'), $clss),
					        'clear'         => false,
					        'id'         	=> $bfield->name,
					        'type'			=> 'number',
					        

					    );

					}

				//show all color fields
				} else if($bfield->type == 'color' && $bfield->is_hide == 0) {

					if($bfield->is_required == 1) {

						$fields['account'][$bfield->name] = array(
					        'label'         => __($bfield->label),
					        'required'      => true,
					        'placeholder'   => _x($bfield->placeholder, 'placeholder', 'eorf'),
					        'class'         => array(($bfield->width == 'full' ? 'full' : 'half'), $clss),
					        'clear'         => false,
					        'id'         	=> $bfield->name,
					        'type'			=> 'text',
					        

					    );

					} else {

						$fields['account'][$bfield->name] = array(
					        'label'         => __($bfield->label, 'eorf'),
					        'placeholder'   => _x($bfield->placeholder, 'placeholder', 'eorf'),
					        'class'         => array(($bfield->width == 'full' ? 'full' : 'half'), $clss),
					        'clear'         => false,
					        'id'         	=> $bfield->name,
					        'type'			=> 'text',
					        

					    );

					}

					?>

					<script>
				
						jQuery("#<?php echo $bfield->name; ?>").spectrum({
						    color: "#000",
						    preferredFormat: "hex",
						});

					</script>

					<?php



				} else if($bfield->is_hide == 0) {

		   			if($bfield->is_required == 1) {

		   				$fields['account'][$bfield->name] = array(
					        'label'         => __($bfield->label),
					        'required'      => true,
					        'placeholder'   => _x($bfield->placeholder, 'placeholder', 'eorf'),
					        'class'         => array(($bfield->width == 'full' ? 'full' : 'half'), $clss),
					        'clear'         => false,
					        'id'         	=> $bfield->name,
					        'type'			=> $bfield->type,
					        

					    );

		   			} else {

		   				$fields['account'][$bfield->name] = array(
					        'label'         => __($bfield->label, 'eorf'),
					        'placeholder'   => _x($bfield->placeholder, 'placeholder', 'eorf'),
					        'class'         => array(($bfield->width == 'full' ? 'full' : 'half'), $clss),
					        'clear'         => false,
					        'id'         	=> $bfield->name,
					        'type'			=> $bfield->type,
					        

					    );

		   			}

				 
				} ?>





        		<script type="text/javascript">
        			
        			jQuery(document).ready(function($) { 

        			
        			
					   <?php
						if($bfield->showif == 'show') { ?>

							<?php if($bfield->pcondition == 'not-empty') { ?>
								$("#registration_field_<?php echo $bfield->condition_field ?>").change(function(){
									$("#<?php echo $bfield->name ?>_field").show();
									$("#<?php echo $bfield->name ?>").show();
								});

								$("#<?php echo $bfield->name ?>_field").show();
								$("#<?php echo $bfield->name ?>").show();

								$('input:radio').change(function() { 

									if ($(this).is(':checked')) { 
										var aa = $(this).val();
							        }

									if(aa != '') {
								 		$("#<?php echo $bfield->name ?>_field").show();
								 		$("#<?php echo $bfield->name ?>").show();
								 	} else {
								 		$("#<?php echo $bfield->name ?>_field").hide();
								 		$("#<?php echo $bfield->name ?>").hide();
								 	}
							 	});

							 	var aa = $("input[type='radio']:checked").val();
							 	if(aa == "<?php echo $bfield->condition_value; ?>") {
							 		$("#<?php echo $bfield->name ?>_field").show();
							 		$("#<?php echo $bfield->name ?>").show();
							 	} else {
							 		$("#<?php echo $bfield->name ?>_field").hide();
							 		$("#<?php echo $bfield->name ?>").hide();
							 	}


							<?php } else if($bfield->pcondition == 'equal-to') { ?>
								$("#registration_field_<?php echo $bfield->condition_field ?>").change(function(){
								 	if($("#registration_field_<?php echo $bfield->condition_field ?>").val() == "<?php echo $bfield->condition_value; ?>") {
								 		$("#<?php echo $bfield->name ?>_field").show();
								 		$("#<?php echo $bfield->name ?>").show();
								 	} else {
								 		$("#<?php echo $bfield->name ?>_field").hide();
								 		$("#<?php echo $bfield->name ?>").hide();
								 	}
								});

								if($("#registration_field_<?php echo $bfield->condition_field ?>").val() == "<?php echo $bfield->condition_value; ?>") {
							 		$("#<?php echo $bfield->name ?>_field").show();
							 		$("#<?php echo $bfield->name ?>").show();
							 	} else {
							 		$("#<?php echo $bfield->name ?>_field").hide();
							 		$("#<?php echo $bfield->name ?>").hide();
							 	}

								$('input:radio').change(function() {

									if ($(this).is(':checked')) {
										var aa = $(this).val();
							        }


									if(aa == "<?php echo $bfield->condition_value; ?>") {
								 		$("#<?php echo $bfield->name ?>_field").show();
								 		$("#<?php echo $bfield->name ?>").show();
								 	} else {
								 		$("#<?php echo $bfield->name ?>_field").hide();
								 		$("#<?php echo $bfield->name ?>").hide();
								 	}
							 	});

							 	var aa = $("input[type='radio']:checked").val();
							 	if(aa == "<?php echo $bfield->condition_value; ?>") {
							 		$("#<?php echo $bfield->name ?>_field").show();
							 		$("#<?php echo $bfield->name ?>").show();
							 	} else {
							 		$("#<?php echo $bfield->name ?>_field").hide();
							 		$("#<?php echo $bfield->name ?>").hide();
							 	}

								

							<?php } else if($bfield->pcondition == 'not-equal-to') { ?>

								$("#registration_field_<?php echo $bfield->condition_field ?>").change(function(){
								 	if($("#registration_field_<?php echo $bfield->condition_field ?>").val() != "<?php echo $bfield->condition_value; ?>") {
								 		$("#<?php echo $bfield->name ?>_field").show();
								 		$("#<?php echo $bfield->name ?>").show();
								 	} else {
								 		$("#<?php echo $bfield->name ?>_field").hide();
								 		$("#<?php echo $bfield->name ?>").hide();
								 	}
								});

								if($("#registration_field_<?php echo $bfield->condition_field ?>").val() != "<?php echo $bfield->condition_value; ?>") {
							 		$("#<?php echo $bfield->name ?>_field").show();
							 		$("#<?php echo $bfield->name ?>").show();
							 	} else {
							 		$("#<?php echo $bfield->name ?>_field").hide();
							 		$("#<?php echo $bfield->name ?>").hide();
							 	}

								$('input:radio').change(function() {

									if ($(this).is(':checked')) {
										var aa = $(this).val();
							        }


									if(aa != "<?php echo $bfield->condition_value; ?>") {
								 		$("#<?php echo $bfield->name ?>_field").show();
								 		$("#<?php echo $bfield->name ?>").show();
								 	} else {
								 		$("#<?php echo $bfield->name ?>_field").hide();
								 		$("#<?php echo $bfield->name ?>").hide();
								 	}
							 	});


							 	var aa = $("input[type='radio']:checked").val();
							 	if(aa != "<?php echo $bfield->condition_value; ?>") {
							 		$("#<?php echo $bfield->name ?>_field").show();
							 		$("#<?php echo $bfield->name ?>").show();
							 	} else {
							 		$("#<?php echo $bfield->name ?>_field").hide();
							 		$("#<?php echo $bfield->name ?>").hide();
							 	}


							<?php } else if($bfield->pcondition == 'checked') { ?>

								$("#registration_field_<?php echo $bfield->condition_field ?>").change(function(){ 

									if ($(this).is(':checked')) {
							           $("#<?php echo $bfield->name ?>_field").show();
							           $("#<?php echo $bfield->name ?>").show();
							        } else {
							        	$("#<?php echo $bfield->name ?>_field").hide();
							        	$("#<?php echo $bfield->name ?>").hide();
							        }
								});

								if ($("#registration_field_<?php echo $bfield->condition_field ?>").is(':checked')) { 
									$("#<?php echo $bfield->name ?>_field").show();
									$("#<?php echo $bfield->name ?>").show();
								} else {
									$("#<?php echo $bfield->name ?>_field").hide();
									$("#<?php echo $bfield->name ?>").hide();
								}



							<?php } ?>



		        			
		        		<?php } elseif($bfield->showif == 'hide') { ?>

		        			<?php if($bfield->pcondition == 'not-empty') { ?>
								$("#registration_field_<?php echo $bfield->condition_field ?>").change(function(){
									$("#<?php echo $bfield->name ?>_field").hide();
									$("#<?php echo $bfield->name ?>").hide();
								});

								$("#<?php echo $bfield->name ?>_field").hide();
								$("#<?php echo $bfield->name ?>").hide();

								
								$('input:radio').change(function() {

									if ($(this).is(':checked')) {
										var aa = $(this).val();
							        }

									if(aa != '') {
								 		$("#<?php echo $bfield->name ?>_field").hide();
								 		$("#<?php echo $bfield->name ?>").hide();
								 	} else {
								 		$("#<?php echo $bfield->name ?>_field").show();
								 		$("#<?php echo $bfield->name ?>").show();
								 	}
							 	});


							 	var aa = $("input[type='radio']:checked").val();
							 	if(aa == "<?php echo $bfield->condition_value; ?>") {
							 		$("#<?php echo $bfield->name ?>_field").hide();
							 		$("#<?php echo $bfield->name ?>").hide();
							 	} else {
							 		$("#<?php echo $bfield->name ?>_field").show();
							 		$("#<?php echo $bfield->name ?>").show();
							 	}


							<?php } else if($bfield->pcondition == 'equal-to') { ?>
								$("#registration_field_<?php echo $bfield->condition_field ?>").change(function(){
								 	if($("#registration_field_<?php echo $bfield->condition_field ?>").val() == "<?php echo $bfield->condition_value; ?>") {
								 		$("#<?php echo $bfield->name ?>_field").hide();
								 		$("#<?php echo $bfield->name ?>").hide();
								 	} else {
								 		$("#<?php echo $bfield->name ?>_field").show();
								 		$("#<?php echo $bfield->name ?>").show();
								 	}
								});

								if($("#registration_field_<?php echo $bfield->condition_field ?>").val() == "<?php echo $bfield->condition_value; ?>") {
							 		$("#<?php echo $bfield->name ?>_field").hide();
							 		$("#<?php echo $bfield->name ?>").hide();
							 	} else {
							 		$("#<?php echo $bfield->name ?>_field").show();
							 		$("#<?php echo $bfield->name ?>").show();
							 	}

								$('input:radio').change(function() {

									if ($(this).is(':checked')) {
										var aa = $(this).val();
							        }


									if(aa == "<?php echo $bfield->condition_value; ?>") {
								 		$("#<?php echo $bfield->name ?>_field").hide();
								 		$("#<?php echo $bfield->name ?>").hide();
								 	} else {
								 		$("#<?php echo $bfield->name ?>_field").show();
								 		$("#<?php echo $bfield->name ?>").show();
								 	}
							 	});

							 	var aa = $("input[type='radio']:checked").val();
							 	if(aa == "<?php echo $bfield->condition_value; ?>") {
							 		$("#<?php echo $bfield->name ?>_field").hide();
							 		$("#<?php echo $bfield->name ?>").hide();
							 	} else {
							 		$("#<?php echo $bfield->name ?>_field").show();
							 		$("#<?php echo $bfield->name ?>").show();
							 	}


								

							<?php } else if($bfield->pcondition == 'not-equal-to') { ?>

								$("#registration_field_<?php echo $bfield->condition_field ?>").change(function(){
								 	if($("#registration_field_<?php echo $bfield->condition_field ?>").val() != "<?php echo $bfield->condition_value; ?>") {
								 		$("#<?php echo $bfield->name ?>_field").hide();
								 		$("#<?php echo $bfield->name ?>").hide();
								 	} else {
								 		$("#<?php echo $bfield->name ?>_field").show();
								 		$("#<?php echo $bfield->name ?>").show();
								 	}
								});

								if($("#registration_field_<?php echo $bfield->condition_field ?>").val() != "<?php echo $bfield->condition_value; ?>") {
							 		$("#<?php echo $bfield->name ?>_field").hide();
							 		$("#<?php echo $bfield->name ?>").hide();
							 	} else {
							 		$("#<?php echo $bfield->name ?>_field").show();
							 		$("#<?php echo $bfield->name ?>").show();
							 	}


								$('input:radio').change(function() {

									if ($(this).is(':checked')) {
										var aa = $(this).val();
							        }


									if(aa != "<?php echo $bfield->condition_value; ?>") {
								 		$("#<?php echo $bfield->name ?>_field").hide();
								 		$("#<?php echo $bfield->name ?>").hide();
								 	} else {
								 		$("#<?php echo $bfield->name ?>_field").show();
								 		$("#<?php echo $bfield->name ?>").show();
								 	}
							 	});

							 	var aa = $("input[type='radio']:checked").val();
							 	if(aa != "<?php echo $bfield->condition_value; ?>") {
							 		$("#<?php echo $bfield->name ?>_field").hide();
							 		$("#<?php echo $bfield->name ?>").hide();
							 	} else {
							 		$("#<?php echo $bfield->name ?>_field").show();
							 		$("#<?php echo $bfield->name ?>").show();
							 	}


								


							<?php } else if($bfield->pcondition == 'checked') { ?>

								$("#registration_field_<?php echo $bfield->condition_field ?>").change(function(){ 

									if ($(this).is(':checked')) {
							           $("#<?php echo $bfield->name ?>_field").hide();
							           $("#<?php echo $bfield->name ?>").hide();
							        } else {
							        	$("#<?php echo $bfield->name ?>_field").show();
							        	$("#<?php echo $bfield->name ?>").show();
							        }
								});


								if ($("#registration_field_<?php echo $bfield->condition_field ?>").is(':checked')) { 
									$("#<?php echo $bfield->name ?>_field").hide();
									$("#<?php echo $bfield->name ?>").hide();
								} else {
									$("#<?php echo $bfield->name ?>_field").show();
									$("#<?php echo $bfield->name ?>").show();
								}



							<?php } ?>
		        			
		        		<?php } else { ?>
		        			
		        		<?php } ?>
					
				    });

        		</script>


		        	<?php 

            }

            $checkout->__set( 'checkout_fields', $fields );
        }

	}

    //validation of fields
	function validate_fields_checkout() { 		    

       	global $woocommerce;

       	if(!is_user_logged_in()) { 
       		if(isset($_POST['createaccount']) && $_POST['createaccount'] == 1) {
                //get all fields
		       	$fields = self::phlox_get_fields();
		       		
                //validation of all fields
		        foreach ($fields as $field) { 

		        	if ( isset( $_POST[$field->name] ) && empty( $_POST[$field->name] ) && ($field->is_required == 1 && $field->showif == '')  ) { 
 
							if($field->type!='captcha') { 

								wc_add_notice( __("<b>".$field->label."</b> ").__( "is required1!", "eorf" ), 'error' );
							}
					}


					if ( isset( $_POST[$field->name] ) && empty( $_POST[$field->name] ) && ($field->is_required == 1 && $field->showif != '') ) { 
						
						if($field->pcondition!='checked') {

							if($_POST[$field->condition_field] == $field->condition_value) { 
		        				
		        				if($field->type!='captcha') {

									wc_add_notice( __("<b>".$field->label."</b> ").__( "is required2!", "eorf" ), 'error' );
								}

		        			}
	        			} 

	        			if($field->pcondition == 'checked' && $_POST[$field->condition_field] == 1) { 

	        				if($field->type!='captcha') {

								wc_add_notice( __("<b>".$field->label."</b> ").__( "is required3!", "eorf" ), 'error' );
							}
	        			}
					}



		        }
        	}
        }		    
	}

}