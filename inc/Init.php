<?php
/**
 * @package PhloxPlugin
 */

namespace Inc;
final class Init{
	//include all classes here
	public static function get_services(){
		return array(
			Pages\Admin::class,
			Base\Enqueue::class,
			Base\SettingsLinks::class,
			Pages\Client::class
		);
	}
	//get all classes and initialize them
	public static function register_services(){
		foreach (self::get_services() as $class) {
			$service = self::instantiate($class);
			if (method_exists($service, 'register')) {
				$service->register();
			}
		}
	}

	private static function instantiate($class){
		$service = new $class();
		return $service;
	}
}
