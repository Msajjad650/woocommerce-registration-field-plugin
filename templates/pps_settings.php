<?php

function get_all_rec(){
	return Inc\Pages\Admin::phlox_get_all_meta_data();
}
$rec_arr = get_all_rec();
$gen_settings = json_decode($rec_arr[0]->meta_data, true);
$fb_settings = json_decode($rec_arr[1]->meta_data, true);
$twitter_settings = json_decode($rec_arr[2]->meta_data, true);
$recaptcha_settings = json_decode($rec_arr[3]->meta_data, true);
$gplus_settings = json_decode($rec_arr[4]->meta_data, true);


?>
<div class="phlox-set">
	<div class="phlox-set-logo">
	    <img class="phlox-set-img" src="<?php echo PLUGIN_URL ?>images/wp_crafts.png">
	    <h2 class="phlox-set-img-heading">Woocommerce Registration Form Custom Fields</h2>
	</div>
	<div class="phlox-set-content">
		<h1>Extension configuration settings</h1>
		<p class="phlox-set-p">
			Configure basic settings to personalize the extension to your website specific requirements. With an enticing user interface, you can easily enable or disable an option or functionality. Try customization the extension and explore the useful features of this extension.
		</p>

		<div class="extendon-support-actions">
			<div class="actions extendon-submit">
				<!-- <input class="button button-primary" type="button" name="" value="Save Changes"> -->
			</div>
		</div>
	</div>
</div>
<div class="tab">
  <button class="tablinks" id="first-tab" onclick="showTab(this, 'general')"><span class="dashicons dashicons-sos"></span>General Settings</button>
  <button class="tablinks" onclick="showTab(this, 'fb')"><span class="dashicons dashicons-facebook"></span>Facebook Settings</button>
  <button class="tablinks" onclick="showTab(this, 'twitter')"><span class="dashicons dashicons-twitter"></span>Twitter Settings</button>
  <button class="tablinks" onclick="showTab(this, 'gplus')" style="display:none;"><span class="dashicons dashicons-googleplus"></span></span>Google Plus Settings</button>
  <button class="tablinks" onclick="showTab(this, 'capcha')"><span class="dashicons dashicons-update"></span>Google reCaptcha Settings</button>
</div>
<form method="post" class="pps-settings-form">
<div id="general" class="tabcontent">
	<h3 class="phlox-content-heading">General Settings</h3>
	<div class="phlox-form-element">
		<div class="phlox-input-label">
			<h3>Account Section Title</h3>
			<span>Main heading of the account section.</span>
		</div>
		<div class="phlox-input-set">
			<input type="text" name="account_title" class="phlox-input-element" value="<?= $gen_settings['account_title'] ?>">
		</div>
	</div>
	<div class="phlox-form-element">
		<div class="phlox-input-label">
			<h3>Profile Section Title</h3>
			<span>Main heading of the profile section.</span>
		</div>
		<div class="phlox-input-set">
			<input type="text" name="profile_title" class="phlox-input-element" value="<?= $gen_settings['profile_title'] ?>">
		</div>
	</div>
</div>

<div id="fb" class="tabcontent">
	<h3 class="phlox-content-heading">Facebook Settings</h3>
	<div class="phlox-form-element">
		<div class="phlox-input-label">
			<h3>Login With Facebook</h3>
			<span>Enable or Disable login with facebook feature on login page.</span>
		</div>
		<div class="phlox-input-set">
			Enabled <input type="radio" name="fb_status" value="enabled" class="phlox-input-element" <?php if($fb_settings['fb_status'] == 'enabled'){ echo 'checked'; } ?>>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			Disabled <input type="radio" name="fb_status" value="disabled" class="phlox-input-element" <?php if($fb_settings['fb_status'] == 'disabled'){ echo 'checked'; } ?>>
		</div>
	</div> 
	<div class="phlox-form-element">
		<div class="phlox-input-label">
			<h3>Facebook App ID</h3>
			<span>This is the facebook app id, you can get this id from your facebook app settings.</span>
		</div>
		<div class="phlox-input-set">
			<input type="text" name="fb_app" class="phlox-input-element" value="<?= $fb_settings['fb_app'] ?>">
		</div>
	</div>
	<div class="phlox-form-element">
		<div class="phlox-input-label">
			<h3>Facebook App Secret</h3>
			<span>This is the facebook app secret, you can get this id from your facebook app settings.</span>
		</div>
		<div class="phlox-input-set">
			<input type="text" name="fb_secret" class="phlox-input-element" value="<?= $fb_settings['fb_secret'] ?>">
		</div>
	</div>
</div>

<div id="twitter" class="tabcontent">
	<h3 class="phlox-content-heading">Twitter Settings</h3>
	<div class="phlox-form-element">
		<div class="phlox-input-label">
			<h3>Login With Twitter</h3>
			<span>Enable or Disable login with twitter feature on login page.</span>
		</div>
		<div class="phlox-input-set">
			Enabled <input type="radio" name="twitter_status" value="enabled" class="phlox-input-element" <?php if($twitter_settings['twitter_status'] == 'enabled'){ echo 'checked'; } ?> >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			Disabled <input type="radio" name="twitter_status" value="disabled" class="phlox-input-element" <?php if($twitter_settings['twitter_status'] == 'disabled'){ echo 'checked'; } ?> >
		</div>
	</div> 
	<div class="phlox-form-element">
		<div class="phlox-input-label">
			<h3>Twitter App Consumer Key</h3>
			<span>This is the twitter app consumer key, you can get this key from your twitter app keys and access token tab.</span>
		</div>
		<div class="phlox-input-set">
			<input type="text" name="twitter_consumer_key" class="phlox-input-element" value="<?= $twitter_settings['twitter_consumer_key'] ?>">
		</div>
	</div>
	<div class="phlox-form-element">
		<div class="phlox-input-label">
			<h3>Twitter App Consumer Secret</h3>
			<span>This is the twitter app consumer secret, you can get this key from your twitter app keys and access token tab.</span>
		</div>
		<div class="phlox-input-set">
			<input type="text" name="twitter_consumer_secret" class="phlox-input-element" value="<?= $twitter_settings['twitter_consumer_secret'] ?>">
		</div>
	</div>
	<div class="phlox-form-element">
		<div class="phlox-input-label">
			<h3>Twitter Callback Url</h3>
			<span>This is the login pages address.</span>
		</div>
		<div class="phlox-input-set">
			<input type="text" name="twitter_callback_url" class="phlox-input-element" value="<?= $twitter_settings['twitter_callback_url'] ?>">
		</div>
	</div>
	<p class="phlox-red">In your app permission you must check "Request email addresses from users" for getting user email, otherwise account will not be created.</p>
</div>

<div id="capcha" class="tabcontent">
	<h3 class="phlox-content-heading">Google reCaptcha Settings</h3>
	<div class="phlox-form-element">
		<div class="phlox-input-label">
			<h3>Site Key</h3>
			<span>Go to <a href="https://www.google.com/recaptcha/intro/index.html">Google reCaptcha</a> site then click on top right reCaptcha button and follow the Instructions. Register you site by giving url path and get the site key.</span>
		</div>
		<div class="phlox-input-set">
			<input type="text" name="recaptcha_site" class="phlox-input-element" value="<?= $recaptcha_settings['recaptcha_site'] ?>">
		</div>
	</div>
	<div class="phlox-form-element">
		<div class="phlox-input-label">
			<h3>Secret Key</h3>
			<span>Go to <a href="https://www.google.com/recaptcha/intro/index.html">Google reCaptcha</a> site then click on top right reCaptcha button and follow the Instructions. Register you site by giving url path and get the secret key.</span>
		</div>
		<div class="phlox-input-set">
			<input type="text" name="recaptcha_secret" class="phlox-input-element" value="<?= $recaptcha_settings['recaptcha_secret'] ?>">
		</div>
	</div>
</div>

<div id="gplus" class="tabcontent">
	<h3 class="phlox-content-heading">Google Plus Settings</h3>
	<div class="phlox-form-element">
		<div class="phlox-input-label">
			<h3>Login With Google Plus</h3>
			<span>Enable or Disable login with Google Plus feature on login page.</span>
		</div>
		<div class="phlox-input-set">
			Enabled <input type="radio" name="gplus_status" value="enabled" class="phlox-input-element" <?php if($gplus_settings['gplus_status'] == 'enabled'){ echo 'checked'; } ?> >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			Disabled <input type="radio" name="gplus_status" value="disabled" class="phlox-input-element" <?php if($gplus_settings['gplus_status'] == 'disabled'){ echo 'checked'; } ?> >
		</div>
	</div>
	<div class="phlox-form-element">
		<div class="phlox-input-label">
			<h3>Client ID</h3>
			<span>Go to <a href="https://console.developers.google.com/apis/dashboard">Google API</a> site to get Client ID.</span>
		</div>
		<div class="phlox-input-set">
			<input type="text" name="gplus_client_id" class="phlox-input-element" value="<?= $gplus_settings['gplus_client_id'] ?>">
		</div>
	</div>
	<div class="phlox-form-element">
		<div class="phlox-input-label">
			<h3>Client Secret</h3>
			<span>Go to <a href="https://console.developers.google.com/apis/dashboard">Google API</a> site to get Client ID.</span>
		</div>
		<div class="phlox-input-set">
			<input type="text" name="gplus_client_secret" class="phlox-input-element" value="<?= $gplus_settings['gplus_client_secret'] ?>">
		</div>
	</div>
	<div class="phlox-form-element">
		<div class="phlox-input-label">
			<h3>Your Login Url</h3>
		</div>
		<div class="phlox-input-set">
			<input type="text" name="gplus_login_url" class="phlox-input-element" value="<?= $gplus_settings['gplus_login_url'] ?>">
			<span>Enter your login url here. And also enter this Url in Authorized redirect URIs when creating OAuth 2.0 client ID.</span>
		</div>
	</div>
</div>
</form>

<div class="phlox-savediv">
	<input type="button" onclick="saveSettings()" value="Save Changes" class="button button-primary right">
</div>
<div class="ajax-loader" style="display:none;">
	<div class="image-laoding">
		<img src="https://www.shop.thegrandb.com/wp-includes/js/tinymce/skins/lightgray/img/loader.gif" title="processing..." alt="loading..."/>
	</div>
	<div class="background-loader"></div>
</div>

<script type="text/javascript">
	jQuery("#first-tab").click();

	function showTab(evt, name) {
	    var i, tabcontent, tablinks;
	    tabcontent = document.getElementsByClassName("tabcontent");
	    for (i = 0; i < tabcontent.length; i++) {
	        tabcontent[i].style.display = "none";
	    }
	    tablinks = document.getElementsByClassName("tablinks");
	    for (i = 0; i < tablinks.length; i++) {
	        tablinks[i].className = tablinks[i].className.replace(" activee", "");
	    }	    
		document.getElementById(name).style.display = "block";
		evt.className += " activee";		
	}

	function  saveSettings() {
		jQuery('.ajax-loader').show();
		var data = jQuery('.pps-settings-form').serialize();
		jQuery.ajax({
			type: 'POST',   
			url: ajaxurl, 
			data: data + '&action=phlox_save_all_settings', 
			success: function(data) {
				jQuery('.ajax-loader').hide();
				if (data == 'done') {
					//alert('Successfully Updated');
					location.reload();
				}else{
					alert('Error');
				}
			}
		});
	}
</script>