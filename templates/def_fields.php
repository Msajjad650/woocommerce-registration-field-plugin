<?php
function get_all_rec(){
	return Inc\Pages\Admin::get_all_default_record();
}
$rec_arr = get_all_rec();
?>
<div class="phloxsol">
	<h2>Default Registration Fields</h2>
	<div class="phlox-right-panel">
		<div class="phlox-right-heading phlox-common-head">
			<h2 class="phlox-heading-h2">Form Fields</h2>
		</div>
		<div class="phlox-right-content">
			<form method="post" class="def-reg-field">
			<?php if(count($rec_arr) > 0){ 
					foreach ($rec_arr as $field) { ?>
						<a class="phlox-collapsible" id="<?= $field->field_id ?>" onclick="toggleField(this)">
							<strong><?php if($field->label != ''){ echo $field->label; } ?><span style="float: right;"><?= ucfirst($field->status) ?></span></strong></a>
						<div class="content" id="<?= $field->field_id ?>">
		                    <p>
		                        <label>Label: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
		                        <input type="text" name="<?= $field->field_id ?>[label]" value="<?= $field->label ?>" class="phlox-input">
		                    </p>
		                    <p>
                    			<label>Required: &nbsp;&nbsp;&nbsp;&nbsp;</label>
                    			<input type="checkbox" value="1" name="<?= $field->field_id ?>[required]" <?php if($field->is_required == '1'){ echo "checked"; } ?>>
                			</p>
                			<p>
                        		<label>Placeholder: </label>
                        		<input type="text" name="<?= $field->field_id ?>[placeholder]" class="phlox-input" value="<?= $field->placeholder ?>">
                    		</p>
                			<p>
                    			<label>Field Width: </label>
			                    <select class="phlox-input" name="<?= $field->field_id ?>[width]">
			                        <option value="full" <?php if($field->width == 'full'){ echo 'selected'; } ?>>Full Width</option>
			                        <option value="half" <?php if($field->width == 'half'){ echo 'selected'; } ?>>Half Width</option>
			                    </select>
                			</p>
                			<p>Status: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
			                    <select class="phlox-input" name="<?= $field->field_id ?>[status]">
			                        <option value="enabled" <?php if($field->status == 'enabled'){ echo 'selected'; } ?>>Enabled</option>
			                        <option value="disabled" <?php if($field->status == 'disabled'){ echo 'selected'; } ?>>Disabled</option>
			                    </select>
                			</p>
                		</div><p></p>
				<?php }
			} ?>
			</form>
		</div>
	</div>
	<div class="phlox-savediv">
		<input type="button" onclick="saveAllFields()" value="Save Changes" class="button button-primary right">
	</div>
</div>
<div class="ajax-loader" style="display:none;">
	<div class="image-laoding">
		<img src="https://www.shop.thegrandb.com/wp-includes/js/tinymce/skins/lightgray/img/loader.gif" title="processing..." alt="loading..."/>
	</div>
	<div class="background-loader"></div>
</div>

<style type="text/css">
	.phlox-collapsible:after{
		display: none;
	}
</style>

<script type="text/javascript">
	function toggleField(cal){
		//var coll = document.getElementById(id);
		cal.classList.toggle("active");
		var content = cal.nextElementSibling;
		if (content.style.maxHeight){
			content.style.maxHeight = null;
		} else {
			content.style.maxHeight = content.scrollHeight + "px";
		} 
	}

	function saveAllFields(){
		jQuery('.ajax-loader').show();
		var data = jQuery('.def-reg-field').serialize();
		jQuery.ajax({
			type: 'POST',   
			url: ajaxurl, 
			data: data + '&action=phlox_save_default_fields', 
			success: function(data) {
				jQuery('.ajax-loader').hide();
				if (data == 'done') {
					//alert('Successfully Updated');
					location.reload();
				}else{
					alert('Error');
				}
			}
		});
	}
</script>