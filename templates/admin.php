<?php

//get all fields data
function get_all_rec(){
	return Inc\Pages\Admin::get_all_record();
}

//get metadata for fields
function get_field_meta($field_id){
	return Inc\Pages\Admin::get_field_meta_data($field_id);
}
$all_record = get_all_rec();

?>
<div class="phloxsol">
	<h2>Registration Fields</h2>
	<div id="show_msg"></div>
	<div class="phlox-left-panel">
		<div class="phlox-left-heading phlox-common-head">
			<h2 class="phlox-heading-h2">Form Fields</h2>
		</div>
		<div class="phlox-left-content">
			<p class="phlox-left-content-btn">
				Text
				<span class="phlox-add-btn button-primary" onclick="addThisField('text')"><i class="fas fa-plus">&nbsp;</i>Add</span>
			</p>
			<p class="phlox-left-content-btn">
				Textarea
				<span class="phlox-add-btn button-primary" onclick="addThisField('textarea')"><i class="fas fa-plus">&nbsp;</i>Add</span>
			</p>
			<p class="phlox-left-content-btn">
				Numeric Field
				<span class="phlox-add-btn button-primary" onclick="addThisField('numeric')"><i class="fas fa-plus">&nbsp;</i>Add</span>
			</p>
			<p class="phlox-left-content-btn">
				Password
				<span class="phlox-add-btn button-primary" onclick="addThisField('password')"><i class="fas fa-plus">&nbsp;</i>Add</span>
			</p>
			<p class="phlox-left-content-btn">
				Select Box
				<span class="phlox-add-btn button-primary" onclick="addThisField('select')"><i class="fas fa-plus">&nbsp;</i>Add</span>
			</p>
			<p class="phlox-left-content-btn">
				Multi Select Box
				<span class="phlox-add-btn button-primary" onclick="addThisField('multiselect')"><i class="fas fa-plus">&nbsp;</i>Add</span>
			</p>
			<p class="phlox-left-content-btn">
				Check Box
				<span class="phlox-add-btn button-primary" onclick="addThisField('checkbox')"><i class="fas fa-plus">&nbsp;</i>Add</span>
			</p>
			<p class="phlox-left-content-btn">
				Check Box (Terms and Conditions)
				<span class="phlox-add-btn button-primary" onclick="addThisField('checkboxa')"><i class="fas fa-plus">&nbsp;</i>Add</span>
			</p>
			<p class="phlox-left-content-btn">
				Radio Button
				<span class="phlox-add-btn button-primary" onclick="addThisField('radio')"><i class="fas fa-plus">&nbsp;</i>Add</span>
			</p>
			<p class="phlox-left-content-btn">
				Date Picker
				<span class="phlox-add-btn button-primary" onclick="addThisField('datepicker')"><i class="fas fa-plus">&nbsp;</i>Add</span>
			</p>
			<p class="phlox-left-content-btn">
				Time Picker
				<span class="phlox-add-btn button-primary" onclick="addThisField('timepicker')"><i class="fas fa-plus">&nbsp;</i>Add</span>
			</p>
			<p class="phlox-left-content-btn">
				Color Picker
				<span class="phlox-add-btn button-primary" onclick="addThisField('color')"><i class="fas fa-plus">&nbsp;</i>Add</span>
			</p>
			<p class="phlox-left-content-btn">
				File / Image Upload
				<span class="phlox-add-btn button-primary" onclick="addThisField('file')"><i class="fas fa-plus">&nbsp;</i>Add</span>
			</p>
			<p class="phlox-left-content-btn">
				Google Captcha
				<span class="phlox-add-btn button-primary" onclick="addThisField('captcha')"><i class="fas fa-plus">&nbsp;</i>Add</span>
			</p>
		</div>
	</div>

	<div class="phlox-right-panel">
		<div class="phlox-right-heading phlox-common-head">
			<h2 class="phlox-heading-h2">Registration Form Fields</h2>
		</div>
		<form method="post" class="reg-fields-form">
			<div class="phlox-right-content sortable">
				<?php if(count($all_record) > 0){ 
					foreach ($all_record as $field) { ?>
						<li id="<?php echo $field->field_id ?>"  style="list-style: none;">
						<a class="phlox-collapsible" id="<?= $field->field_id ?>" onclick="toggleField(this)"><?php if($field->label != ''){ echo $field->label; }else{ ?>Registration Field <?php echo $field->field_id; } ?></a>
		                <div class="content" id="<?= $field->field_id ?>">
		                    <p>
		                        <label>Label: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
		                        <input type="text" name="<?= $field->field_id ?>[label]" value="<?= $field->label ?>" class="phlox-input">
		                    </p>
			                <?php if($field->type == 'checkboxa'){ ?>
	                			<p>
	                			Note: This field is for just user acceptance like terms and conditions, this field will not be saved or sent in order or emails etc. You can add anchor tag to link with other page.
	                			</p>
	                			<p>
	                    			<label>Required: </label>
	                    			<input type="checkbox" value="1" name="<?= $field->field_id ?>[required]" <?php if($field->is_required == '1'){ echo "checked"; } ?>>
	                			</p>
	                			<p>
	                    			<label>Text: </label><br>
	                    			<textarea name="<?= $field->field_id ?>[checkbox_text]" style="width: 100%;">
	                    				<?= $field->checkbox_text ?>
	                    			</textarea>
	                			</p>
	            			<?php }else{ ?>
	                			<p>
	                    			<label>Required: </label>
	                    			<input type="checkbox" value="1" name="<?= $field->field_id ?>[required]" <?php if($field->is_required == '1'){ echo "checked"; } ?>>
	                			</p>
	                			<p>
	                    			<label>Hide: </label>
	                    			<input type="checkbox" value="1" name="<?= $field->field_id ?>[hidden]" <?php if($field->is_hide == '1'){ echo "checked"; } ?>>
	                			</p>
	                			<p>
	                    			<label>Read Only(Do not allow customers to edit in myaccount): </label>
	                    			<input type="checkbox" value="1" name="<?= $field->field_id ?>[readonly]" <?php if($field->is_readonly == '1'){ echo "checked"; } ?>>
	                			</p>
	            			<?php }
	            			if($field->type == 'select' || $field->type == 'multiselect' || $field->type == 'radio'){ ?>
	                			<div class="phlox_field_wrapper">
	                    			<div id="phlox-options-<?= $field->field_id ?>">
	                    				<?php $get_rec = get_field_meta($field->field_id);
	                    				if(isset($get_rec[0]->meta_data)){
    	                    				$option_rec = json_decode($get_rec[0]->meta_data, true);
    	                    				$i = 0;
    	                    				foreach ($option_rec as $text => $value) {
    	                    					if($i==0){ ?>
    	                    					<input class="phlox-optval-input" placeholder="Option Value" type="text" name="<?= $field->field_id ?>[optionValue][]" value="<?= $value ?>">
    		                        			<input class="phlox-optval-input" placeholder="Option Text" type="text" name="<?= $field->field_id ?>[optionText][]" value="<?= $text ?>">
    		                        			<i class="fas fa-plus-circle fa-2x phlox-font-awesome" onclick="addOption(<?= $field->field_id ?>)"></i>	
    			                        	<?php }else{ ?>
    		                        			<input class="phlox-optval-input" id="<?= $field->field_id.$i ?>" placeholder="Option Value" type="text" name="<?= $field->field_id ?>[optionValue][]" value="<?= $value ?>"> 
    		                        			<input class="phlox-optval-input" id="<?= $field->field_id.$i ?>" placeholder="Option Text" type="text" name="<?= $field->field_id ?>[optionText][]" value="<?= $text ?>"> 
    		                        			<i class="fas fa-minus-circle fa-2x phlox-font-awesome" id="<?= $field->field_id.$i ?>" onclick="delOption(<?= $field->field_id.$i ?>)"></i>
    			                        	<?php } $i++;
    	                    				}
	                    				}else{ ?>
	                    				    <input class="phlox-optval-input" placeholder="Option Value" type="text" name="<?= $field->field_id ?>[optionValue][]">
		                        			<input class="phlox-optval-input" placeholder="Option Text" type="text" name="<?= $field->field_id ?>[optionText][]">
		                        			<i class="fas fa-plus-circle fa-2x phlox-font-awesome" onclick="addOption(<?= $field->field_id ?>)"></i>
	                    				<?php } ?>
	                    			</div>
	                			</div>
	            			<?php }else{
		                		if($field->type != 'checkbox' && $field->type != 'checkboxa'){ ?>
		                    		<p>
		                        		<label>Placeholder: </label>
		                        		<input type="text" name="<?= $field->field_id ?>[placeholder]" class="phlox-input" value="<?= $field->placeholder ?>">
		                    		</p>
		                		<?php }
		            		} ?>
                			<p>
                    			<label>Field Width: </label>
			                    <select class="phlox-input" name="<?= $field->field_id ?>[width]">
			                        <option value="full" <?php if($field->width == 'full'){ echo 'selected'; } ?>>Full Width</option>
			                        <option value="half" <?php if($field->width == 'half'){ echo 'selected'; } ?>>Half Width</option>
			                    </select>
                			</p>
                			<p>
                    			<label>Conditional Logic: </label>
			                    <div>
			                        <select name="<?= $field->field_id ?>[showif]">
			                            <option>Select</option>
			                            <option value="show" <?php if($field->showif == 'show'){ echo 'selected'; } ?>>Show</option>
			                            <option value="hide" <?php if($field->showif == 'hide'){ echo 'selected'; } ?>>Hide</option>
			                        </select>
			                        <strong>If value of </strong>
			                        <select name="<?= $field->field_id ?>[conditionField]" class="phlox-fcond">
			                            <option>Select</option>
			                            <?php foreach($all_record as $all_rec){
			                            	if ($all_rec->field_id == $field->field_id) { continue; } ?>
			                            	<option value="<?= $all_rec->field_id ?>" <?php if($field->condition_field == $all_rec->field_id){ echo 'selected'; } ?>><?php if($all_rec->label != ''){ echo $all_rec->label; }else{ echo $all_rec->name; } ?></option>
			                            <?php } ?>
			                        </select>
			                        <select name="<?= $field->field_id ?>[condition]">
			                            <option value="">Select</option>
			                            <option value="not-empty" <?php if($field->pcondition == 'not-empty'){ echo 'selected'; } ?>>is not empty</option>
			                            <option value="equal-to" <?php if($field->pcondition == 'equal-to'){ echo 'selected'; } ?>>is equal to</option>
			                            <option value="not-equal-to" <?php if($field->pcondition == 'not-equal-to'){ echo 'selected'; } ?>>is not equal to</option>
			                            <option value="checked" <?php if($field->pcondition == 'checked'){ echo 'selected'; } ?>>is checked</option>                         
			                        </select>
			                        <input type="text" name="<?= $field->field_id ?>[conditionValue]" value="<?= $field->condition_value ?>">
			                    </div>
                			</p>
                			<p><a href="javascript:void(0)" onclick="deleteThis(<?= $field->field_id ?>)">Delete</a></p>
                			<p id="<?= $field->field_id ?>"><input type="hidden" name="<?= $field->field_id ?>[type]" value="<?= $field->type ?>" /></p>
            			</div>  
            			</li>          			
					<?php }
				} ?>
			</div>
		</form>
	</div>

	<div class="phlox-savediv">
		<input type="button" onclick="saveAllFields()" value="Save Changes" class="button button-primary right">
	</div>
</div>

<div class="ajax-loader" style="display:none;">
	<div class="image-laoding">
		<img src="<?php echo PLUGIN_URL ?>images/loader.gif" title="processing..." alt="loading..."/>
	</div>
	<div class="background-loader"></div>
</div>
<script type="text/javascript">

jQuery( function() {
	jQuery( ".sortable" ).sortable({
		start: function(e, ui) {
	    }, 
		update: function( event, ui ) {
			var ajaxurl = "<?php echo admin_url( 'admin-ajax.php'); ?>";
			var order = jQuery(this).sortable('toArray');
			jQuery.ajax({
				type: 'POST', 
				url: ajaxurl,
				data: {"action": "phlox_update_sortorder","fieldids":order}, 
				success: function(data){ 
				}
			});                                                            
		}
	});
	jQuery( ".sortable" ).disableSelection();
});

function add_collapsible(classs){
	var coll = document.getElementById(classs);
	var i;
	coll.addEventListener("click", function() {
		this.classList.toggle("active");
		var content = this.nextElementSibling;
		if (content.style.maxHeight){
			content.style.maxHeight = null;
		} else {
			content.style.maxHeight = "2000px";
		} 
	});	
}

function toggleField(cal){
	cal.classList.toggle("active");
	var content = cal.nextElementSibling;
	if (content.style.maxHeight){
		content.style.maxHeight = null;
	} else {
		content.style.maxHeight = "2000px";
	} 
}

var ajaxurl = "<?php echo admin_url( 'admin-ajax.php'); ?>";
var count = 1;
function addThisField(fieldType) {
	jQuery('.ajax-loader').show();
	jQuery.ajax({
		type: 'POST',   
		url: ajaxurl, 
		data: {"action": "phlox_insert_field","fieldtype":fieldType,"type":"registration","name":"registration_text","mode":"registration_additional"}, 
		dataType: 'json',
		success: function(data) {
			if(data.status == 'ok'){
				jQuery('.ajax-loader').hide();
				jQuery(".phlox-right-content").append(data.html);
				add_collapsible(data.classid);
			}else{
				jQuery("#show_msg").html('<div class="error notice"><p><strong>Error! </strong>You have already added Google Captcha!</p></div><br>');
				jQuery("html").scrollTop(0);
				setTimeout(function(){ 
					jQuery("#show_msg").html(""); 
				}, 7000);
			}
			jQuery('.ajax-loader').hide();
		}
	});
}

function deleteThis(id){
	jQuery('.ajax-loader').show();
	jQuery.ajax({
		type: 'POST',   
		url: ajaxurl, 
		data: {"action": "phlox_delete_field","fieldId":id}, 
		success: function(data) {
			jQuery('.ajax-loader').hide();
			if (data == 'ok') {
				jQuery("#"+id).remove();
				jQuery("#"+id).remove();
				jQuery("#show_msg").html('<div class="updated notice is-dismissible"><p>Successfully Deleted!</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div><br>');
				jQuery("html").scrollTop(0);
				setTimeout(function(){ 
					jQuery("#show_msg").html(""); 
				}, 7000);
			}else{
				alert('Error0');
			}
		}
	});
}

function addOption(id){
	jQuery("#phlox-options-"+id).append('<input class="phlox-optval-input" id="'+id+count+'" placeholder="Option Value" type="text" name="'+id+'[optionValue][]"> <input class="phlox-optval-input" id="'+id+count+'" placeholder="Option Text" type="text" name="'+id+'[optionText][]"> <i class="fas fa-minus-circle fa-2x phlox-font-awesome" id="'+id+count+'" onclick="delOption('+id+count+')">');
	count++;
}

function delOption(id){
	jQuery("#"+id).remove();
	jQuery("#"+id).remove();
	jQuery("#"+id).remove();
}

function saveAllFields(){
	jQuery('.ajax-loader').show();
	var ajaxurl = "<?php echo admin_url( 'admin-ajax.php'); ?>";
	var order = jQuery(".sortable").sortable('toArray');
	// alert(order);
	jQuery.ajax({
		type: 'POST',   
		url: ajaxurl, 
		data: {"action": "phlox_update_sortorder","fieldids":order}, 
		success: function(data){ 
		}
	});

	var data = jQuery('.reg-fields-form').serialize();
	jQuery.ajax({
		type: 'POST',   
		url: ajaxurl, 
		data: data + '&action=phlox_save_all_fields', 
		success: function(data) {
			jQuery('.ajax-loader').hide();
			if (data == 'done') {
				jQuery("#show_msg").html('<div class="updated notice is-dismissible"><p>Successfully Saved!</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div><br>');
				jQuery("html").scrollTop(0);
				setTimeout(function(){ 
				    location.reload();
					jQuery("#show_msg").html(""); 
				}, 2000);
				//alert('Successfully Updated');
			}else{
				alert('Error');
			}
		}
	});
}



</script>