<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit4c587891b5835b0a5c18323bed46ff32
{
    public static $prefixLengthsPsr4 = array (
        'I' => 
        array (
            'Inc\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Inc\\' => 
        array (
            0 => __DIR__ . '/../..' . '/inc',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit4c587891b5835b0a5c18323bed46ff32::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit4c587891b5835b0a5c18323bed46ff32::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
